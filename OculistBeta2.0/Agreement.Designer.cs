﻿namespace OculistBeta2._0
{
    partial class Agreement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /﻿namespace OculistBeta2._0
{
    partial class Agreement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Agreement));
            this.AgreementText = new System.Windows.Forms.Label();
            this.ButtonAgree = new System.Windows.Forms.Button();
            this.ButtonDisagree = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AgreementText
            // 
            this.AgreementText.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.AgreementText.Location = new System.Drawing.Point(13, 13);
            this.AgreementText.Name = "AgreementText";
            this.AgreementText.Size = new System.Drawing.Size(459, 209);
            this.AgreementText.TabIndex = 0;
            this.AgreementText.Text = resources.GetString("AgreementText.Text");
            // 
            // ButtonAgree
            // 
            this.ButtonAgree.Location = new System.Drawing.Point(20, 225);
            this.ButtonAgree.Name = "ButtonAgree";
            this.ButtonAgree.Size = new System.Drawing.Size(157, 55);
            this.ButtonAgree.TabIndex = 1;
            this.ButtonAgree.Text = "Agree";
            this.ButtonAgree.UseVisualStyleBackColor = true;
            this.ButtonAgree.Click += new System.EventHandler(this.ButtonAgree_Click);
            // 
            // ButtonDisagree
            // 
            this.ButtonDisagree.Location = new System.Drawing.Point(315, 225);
            this.ButtonDisagree.Name = "ButtonDisagree";
            this.ButtonDisagree.Size = new System.Drawing.Size(157, 55);
            this.ButtonDisagree.TabIndex = 2;
            this.ButtonDisagree.Text = "Decline";
            this.ButtonDisagree.UseVisualStyleBackColor = true;
            this.ButtonDisagree.Click += new System.EventHandler(this.ButtonDisagree_Click);
            // 
            // Agreement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 292);
            this.Controls.Add(this.ButtonDisagree);
            this.Controls.Add(this.ButtonAgree);
            this.Controls.Add(this.AgreementText);
            this.Name = "Agreement";
            this.Text = "Agreement";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label AgreementText;
        private System.Windows.Forms.Button ButtonAgree;
        private System.Windows.Forms.Button ButtonDisagree;
    }
}