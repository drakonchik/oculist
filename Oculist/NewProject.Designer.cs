﻿namespace Oculist
{
    partial class NewProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ProjectName = new System.Windows.Forms.TextBox();
            this.CreateNewProjectButton = new System.Windows.Forms.Button();
            this.CancelNewProject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13)﻿namespace Oculist
{
    partial class NewProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ProjectName = new System.Windows.Forms.TextBox();
            this.CreateNewProjectButton = new System.Windows.Forms.Button();
            this.CancelNewProject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Project Name";
            // 
            // ProjectName
            // 
            this.ProjectName.Location = new System.Drawing.Point(39, 42);
            this.ProjectName.Name = "ProjectName";
            this.ProjectName.Size = new System.Drawing.Size(189, 20);
            this.ProjectName.TabIndex = 1;
            this.ProjectName.Text = "New Projecty";
            // 
            // CreateNewProjectButton
            // 
            this.CreateNewProjectButton.Location = new System.Drawing.Point(39, 69);
            this.CreateNewProjectButton.Name = "CreateNewProjectButton";
            this.CreateNewProjectButton.Size = new System.Drawing.Size(75, 23);
            this.CreateNewProjectButton.TabIndex = 2;
            this.CreateNewProjectButton.Text = "Create";
            this.CreateNewProjectButton.UseVisualStyleBackColor = true;
            this.CreateNewProjectButton.Click += new System.EventHandler(this.CreateNewProjectButton_Click);
            // 
            // CancelNewProject
            // 
            this.CancelNewProject.Location = new System.Drawing.Point(153, 69);
            this.CancelNewProject.Name = "CancelNewProject";
            this.CancelNewProject.Size = new System.Drawing.Size(75, 23);
            this.CancelNewProject.TabIndex = 3;
            this.CancelNewProject.Text = "Cancel";
            this.CancelNewProject.UseVisualStyleBackColor = true;
            this.CancelNewProject.Click += new System.EventHandler(this.CancelNewProject_Click);
            // 
            // NewProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 116);
            this.Controls.Add(this.CancelNewProject);
            this.Controls.Add(this.CreateNewProjectButton);
            this.Controls.Add(this.ProjectName);
            this.Controls.Add(this.label1);
            this.Name = "NewProject";
            this.Text = "NewProject";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ProjectName;
        private System.Windows.Forms.Button CreateNewProjectButton;
        private System.Windows.Forms.Button CancelNewProject;
    }
}