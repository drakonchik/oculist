﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OculistBeta2._0
{
    class Recordkeeper
    {

        /// <summary>
        /// This cl﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OculistBeta2._0
{
    class Recordkeeper
    {

        /// <summary>
        /// This class maintains all eye Tracking information and stores them in a CSV file
        /// </summary>
        private List<int> eyeX, eyeY, pointNr;
        private List<int> lEyeX, lEyeY, rEyeX, rEyeY;
        private List<string> SeenImage, SeenText;

        private string CSVfilename;
        

        public Recordkeeper()
        {


            eyeX = new List<int>();
            eyeY = new List<int>();
            lEyeX = new List<int>();
            lEyeY = new List<int>();
            rEyeX = new List<int>();
            rEyeY = new List<int>();
            SeenImage = new List<string>();
            SeenText = new List<string>();

        }

        // Update(bothEyesX, bothEyesY, leftEyeX, leftEyeY, rightEyeX, rightEyeY, recordLeftEye, recordRightEye
        public void Update(int x,int y, int lx, int ly, int rx, int ry, bool left, bool right, bool comparatorFoundImage, bool comparatorFoundText, string foundPoint, bool pause, bool endRecording)
        {
            if(!pause)
            {
                eyeX.Add(x);
                eyeY.Add(y);

                if (left == true)
                {
                    lEyeX.Add(lx);
                    lEyeY.Add(ly);
                }

                if (right == true)
                {
                    rEyeX.Add(rx);
                    rEyeY.Add(ry);
                }

                if (comparatorFoundImage)
                {
                    SeenImage.Add(foundPoint);
                }
                else
                {
                    SeenImage.Add("NO_ENTRY");
                }

                if (comparatorFoundText)
                {
                    SeenText.Add(foundPoint);
                }
                else
                {
                    SeenImage.Add("NO_ENTRY");
                }
            }
        }

        public void recordEyes(int X, int Y)
        {
            eyeX.Add(X);
            eyeY.Add(Y);
        }

        public void saveRecordingToCSV2()
        {

            StreamWriter writers = new StreamWriter("Andris.csv");

                for (int i = 0; i < eyeX.Count; i++)
                {
                    writers.Write(eyeX[i] + ",");
                    writers.WriteLine(eyeY[i]);

                    if (SeenText.Equals(null))
                    {
                        //writers.WriteLine(SeenImage[i]);
                    }
                    else
                    {
                        //writers.WriteLine(SeenImage[i]);
                    }

                }
            }

        public void saveRecordingToCSV(MainWindow mainy, string path, string name)
        {
            CSVfilename = name;

            if(CSVfilename.Equals("User001.csv"))
            {
                mainy.AddTextForMessageBox("File saved with default name User001.csv, please add users name next time!\n");
            }
            else
            {
                mainy.AddTextForMessageBox("Tracking session successful, data saved in Data/Output folder!\n");
            }

            StreamWriter writers = new StreamWriter(path + CSVfilename);

            if(lEyeX.Equals(null) && rEyeX.Equals(null)) // record only raw data
            {
                for(int i = 0; i < eyeX.Count; i++)
                {
                    writers.Write(eyeX[i]);
                    writers.Write(eyeY[i]);

                    if(SeenText.Equals(null))
                    {
                        writers.WriteLine(SeenImage[i]);
                    }
                    else
                    {
                        writers.WriteLine(SeenImage[i]);
                    }
                    
                }
            }
            else if(lEyeX.Equals(null)) // record right eye data
            {
                for (int i = 0; i < eyeX.Count; i++)
                {
                    writers.Write(eyeX[i]);
                    writers.Write(eyeY[i]);
                    writers.Write(rEyeX[i]);
                    writers.Write(rEyeY[i]);

                    if (SeenText.Equals(null))
                    {
                        writers.WriteLine(SeenImage[i]);
                    }
                    else
                    {
                        writers.WriteLine(SeenImage[i]);
                    }

                }
            }
            else if(rEyeX.Equals(null)) // record left eye data
            {
                for (int i = 0; i < eyeX.Count; i++)
                {
                    writers.Write(eyeX[i]);
                    writers.Write(eyeY[i]);
                    writers.Write(lEyeX[i]);
                    writers.Write(lEyeY[i]);

                    if (SeenText.Equals(null))
                    {
                        writers.WriteLine(SeenImage[i]);
                    }
                    else
                    {
                        writers.WriteLine(SeenImage[i]);
                    }

                }
            }
            else // record raw data and both eyes
            {
                for (int i = 0; i < eyeX.Count; i++)
                {
                    writers.Write(eyeX[i]);
                    writers.Write(eyeY[i]);
                    writers.Write(lEyeX[i]);
                    writers.Write(lEyeY[i]);
                    writers.Write(rEyeX[i]);
                    writers.Write(rEyeY[i]);

                    if (SeenText.Equals(null))
                    {
                        writers.WriteLine(SeenImage[i]);
                    }
                    else
                    {
                        writers.WriteLine(SeenImage[i]);
                    }

                }
            }
        }
    }
}
