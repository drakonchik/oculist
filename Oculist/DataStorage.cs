﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Oculist.Structure
{
    public class PointDataIMG
    {

        static readonly int maxSize = 0x64;
        // Array of cordinates of X and Y of Images
        public int[,] PositionX = new int[maxSize, maxSize];
        public int[,] PositionY = new int[maxSize, maxSize]; // image ID position; Image ID, position Y;
        public List<Image> images = new List<Image>();
        public List<string> imageNames = new List<string>();
        public bool[,] PointsChecked = new bool[maxSize, maxSize]; // image ID,point ID

        public void reshufleData(int index)
        {
            for(int i = index; i < maxSize - 0x01; i++)
            {
                for( int j = 0x00; j < maxSize; j++)
                {
                    PositionX[i, j] = PositionX[i + 0x01, j];
                    PositionY[i, j] = PositionY[i + 0x01, j];
                    PointsChecked[i, j] = PointsChecked[i + 0x01, j];
                }
            }
        }

        public void swapData(int index1, int index2)
        {
            int dummy;
            bool booly;
            for (int j = 0x00; j < maxSize; j++)
            {
                dummy = PositionX[index1, j];
                PositionX[index1, j] = PositionX[index2, j];
                PositionX[index2, j] = dummy;

                dummy = PositionY[index1, j];
                PositionY[index1, j] = PositionY[index2, j];
                PositionY[index2, j] = dummy;

                booly = PointsChecked[index1, j];
                PointsChecked[index1, j] = PointsChecked[index2, j];
                PointsChecked[index2, j] = booly;
            }
        }

        public void clearData()
        {
            images.Clear();
            imageNames.Clear();
            Array.Clear(PositionX, 0x00, PositionX.Length);
            Array.Clear(PositionY, 0x00, PositionY.Length);
            Array.Clear(PointsChecked, 0x00, PointsChecked.Length);
        }


    }

    public class PointDataTXT
    {
        static readonly int maxSize = 0x64;
        // Array of cordinates of X and Y of Text
        public int[,] PositionX = new int[maxSize, maxSize];
        public int[,] PositionY = new int[maxSize, maxSize];
        public List<string> text = new List<string>();
        public List<string> textNames = new List<string>();
        public bool[,] PointsChecked = new bool[maxSize, maxSize]; // text ID,point ID


        public void reshufleData(int index)
        {
            for (int i = index; i < maxSize - 0x01; i++)
            {
                for (int j = 0x00; j < maxSize; j++)
                {
                    PositionX[i, j] = PositionX[i + 0x01, j];
                    PositionY[i, j] = PositionY[i + 0x01, j];
                    PointsChecked[i, j] = PointsCh﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Oculist.Structure
{
    public class PointDataIMG
    {

        static readonly int maxSize = 0x64;
        // Array of cordinates of X and Y of Images
        public int[,] PositionX = new int[maxSize, maxSize];
        public int[,] PositionY = new int[maxSize, maxSize]; // image ID position; Image ID, position Y;
        public List<Image> images = new List<Image>();
        public List<string> imageNames = new List<string>();
        public bool[,] PointsChecked = new bool[maxSize, maxSize]; // image ID,point ID

        public void reshufleData(int index)
        {
            for(int i = index; i < maxSize - 0x01; i++)
            {
                for( int j = 0x00; j < maxSize; j++)
                {
                    PositionX[i, j] = PositionX[i + 0x01, j];
                    PositionY[i, j] = PositionY[i + 0x01, j];
                    PointsChecked[i, j] = PointsChecked[i + 0x01, j];
                }
            }
        }

        public void swapData(int index1, int index2)
        {
            int dummy;
            bool booly;
            for (int j = 0x00; j < maxSize; j++)
            {
                dummy = PositionX[index1, j];
                PositionX[index1, j] = PositionX[index2, j];
                PositionX[index2, j] = dummy;

                dummy = PositionY[index1, j];
                PositionY[index1, j] = PositionY[index2, j];
                PositionY[index2, j] = dummy;

                booly = PointsChecked[index1, j];
                PointsChecked[index1, j] = PointsChecked[index2, j];
                PointsChecked[index2, j] = booly;
            }
        }

        public void clearData()
        {
            images.Clear();
            imageNames.Clear();
            Array.Clear(PositionX, 0x00, PositionX.Length);
            Array.Clear(PositionY, 0x00, PositionY.Length);
            Array.Clear(PointsChecked, 0x00, PointsChecked.Length);
        }


    }

    public class PointDataTXT
    {
        static readonly int maxSize = 0x64;
        // Array of cordinates of X and Y of Text
        public int[,] PositionX = new int[maxSize, maxSize];
        public int[,] PositionY = new int[maxSize, maxSize];
        public List<string> text = new List<string>();
        public List<string> textNames = new List<string>();
        public bool[,] PointsChecked = new bool[maxSize, maxSize]; // text ID,point ID


        public void reshufleData(int index)
        {
            for (int i = index; i < maxSize - 0x01; i++)
            {
                for (int j = 0x00; j < maxSize; j++)
                {
                    PositionX[i, j] = PositionX[i + 0x01, j];
                    PositionY[i, j] = PositionY[i + 0x01, j];
                    PointsChecked[i, j] = PointsChecked[i + 0x01, j];
                }
            }
        }

        public void swapData(int index1, int index2)
        {
            int dummy;
            bool booly;
            for (int j = 0x00; j < maxSize; j++)
            {
                dummy = PositionX[index1, j];
                PositionX[index1, j] = PositionX[index2, j];
                PositionX[index2, j] = dummy;

                dummy = PositionY[index1, j];
                PositionY[index1, j] = PositionY[index2, j];
                PositionY[index2, j] = dummy;

                booly = PointsChecked[index1, j];
                PointsChecked[index1, j] = PointsChecked[index2, j];
                PointsChecked[index2, j] = booly;
            }
        }

        public void clearData()
        {
            text.Clear();
            textNames.Clear();
            Array.Clear(PositionX, 0x00, PositionX.Length);
            Array.Clear(PositionY, 0x00, PositionY.Length);
            Array.Clear(PointsChecked, 0x00, PointsChecked.Length);
        }


    }
}
