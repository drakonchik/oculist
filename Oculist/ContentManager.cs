﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;

namespace Oculist
{
    class ContentManager 
    {
        OpenFileDialog openFileDialog = new OpenFileDialog();
        string MainFileLocation = System.Windows.Forms.Application.StartupPath;
        string projectname;

        String FileName;
        public ContentManager()
        {

        }

        public Bitmap LoadImageContent()
        {
            projectname = Form1.PROJECTNAME;
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "png files (*.png)|*.png|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {            
                FileName = openFileDialog.FileName;
                FileName = Path.GetFileName(FileName);
                return new Bitmap(openFileDialog.FileName);
            }
            else
            {
                return null;
            }
            
        }

        public string LoadTextContent()
        {
            
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string text;
                FileName = openFileDialog.FileName;
                FileName = Path.GetFileName(FileName);
                using (StreamReader reader = new StreamReader(openFileDialog.FileName))
                {
                     text = reader.ReadToEnd();
                }
                return text;
            }
            else
            {
                return null;
            }

        }

        public Document LoadWordContent(Microsoft.Office.Interop.Word.Application app)
        {
            Document doc;
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "docx files (*.docx*.docx";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileName = openFileDialog.FileName;
                doc = app.Documents.Open(FileName);
                FileName = Path.GetFileName(FileName);
                return doc;

            }
            else
            {
                return null;
            }

        }

        public string getFileName()
        {
        return FileName;
        }

        public void SaveContent(string path)
        {

        }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;

namespace Oculist
{
    class ContentManager 
    {
        OpenFileDialog openFileDialog = new OpenFileDialog();
        string MainFileLocation = System.Windows.Forms.Application.StartupPath;
        string projectname;

        String FileName;
        public ContentManager()
        {

        }

        public Bitmap LoadImageContent()
        {
            projectname = Form1.PROJECTNAME;
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "png files (*.png)|*.png|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {            
                FileName = openFileDialog.FileName;
                FileName = Path.GetFileName(FileName);
                return new Bitmap(openFileDialog.FileName);
            }
            else
            {
                return null;
            }
            
        }

        public string LoadTextContent()
        {
            
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string text;
                FileName = openFileDialog.FileName;
                FileName = Path.GetFileName(FileName);
                using (StreamReader reader = new StreamReader(openFileDialog.FileName))
                {
                     text = reader.ReadToEnd();
                }
                return text;
            }
            else
            {
                return null;
            }

        }

        public Document LoadWordContent(Microsoft.Office.Interop.Word.Application app)
        {
            Document doc;
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "docx files (*.docx*.docx";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileName = openFileDialog.FileName;
                doc = app.Documents.Open(FileName);
                FileName = Path.GetFileName(FileName);
                return doc;

            }
            else
            {
                return null;
            }

        }

        public string getFileName()
        {
        return FileName;
        }

        public void SaveContent(string path)
        {

        }
    }
}
