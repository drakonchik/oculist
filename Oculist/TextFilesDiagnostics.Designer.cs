﻿namespace Oculist
{
    partial class TextFilesDiagnostics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SeenOrNotSeen = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.XandYPositions = new System.Windows.Forms.ListBox();
            this.gr﻿namespace Oculist
{
    partial class TextFilesDiagnostics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SeenOrNotSeen = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.XandYPositions = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TextFiles = new System.Windows.Forms.ListBox();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.SeenOrNotSeen);
            this.groupBox3.Location = new System.Drawing.Point(424, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 425);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Seen or Not-Seen";
            // 
            // SeenOrNotSeen
            // 
            this.SeenOrNotSeen.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.SeenOrNotSeen.FormattingEnabled = true;
            this.SeenOrNotSeen.Location = new System.Drawing.Point(6, 14);
            this.SeenOrNotSeen.Name = "SeenOrNotSeen";
            this.SeenOrNotSeen.Size = new System.Drawing.Size(188, 407);
            this.SeenOrNotSeen.TabIndex = 18;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.XandYPositions);
            this.groupBox2.Location = new System.Drawing.Point(218, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 425);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "X and Y Positions";
            // 
            // XandYPositions
            // 
            this.XandYPositions.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.XandYPositions.FormattingEnabled = true;
            this.XandYPositions.Location = new System.Drawing.Point(6, 14);
            this.XandYPositions.Name = "XandYPositions";
            this.XandYPositions.Size = new System.Drawing.Size(188, 407);
            this.XandYPositions.TabIndex = 17;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TextFiles);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 425);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Text Files";
            // 
            // TextFiles
            // 
            this.TextFiles.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.TextFiles.FormattingEnabled = true;
            this.TextFiles.Location = new System.Drawing.Point(6, 14);
            this.TextFiles.Name = "TextFiles";
            this.TextFiles.Size = new System.Drawing.Size(188, 407);
            this.TextFiles.TabIndex = 16;
            this.TextFiles.SelectedIndexChanged += new System.EventHandler(this.ImageFiles_SelectedIndexChanged);
            // 
            // TextFilesDiagnostics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 450);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "TextFilesDiagnostics";
            this.Text = "TextFilesDiagnostics";
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox SeenOrNotSeen;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox XandYPositions;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox TextFiles;
    }
}