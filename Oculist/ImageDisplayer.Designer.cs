﻿namespace Oculist
{
    partial class ImageDisplayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windo﻿namespace Oculist
{
    partial class ImageDisplayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImageDisplayerImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ImageDisplayerImage)).BeginInit();
            this.SuspendLayout();
            // 
            // ImageDisplayerImage
            // 
            this.ImageDisplayerImage.Location = new System.Drawing.Point(3, 1);
            this.ImageDisplayerImage.Name = "ImageDisplayerImage";
            this.ImageDisplayerImage.Size = new System.Drawing.Size(1024, 690);
            this.ImageDisplayerImage.TabIndex = 0;
            this.ImageDisplayerImage.TabStop = false;
            // 
            // ImageDisplayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1027, 692);
            this.Controls.Add(this.ImageDisplayerImage);
            this.Name = "ImageDisplayer";
            this.Text = "ImageDisplayer";
            this.Load += new System.EventHandler(this.ImageDisplayer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ImageDisplayerImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox ImageDisplayerImage;
    }
}