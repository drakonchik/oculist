﻿namespace OculistBeta2._0
{
    partial class ImageTracking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Win﻿namespace OculistBeta2._0
{
    partial class ImageTracking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TrackingPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.TrackingPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // TrackingPictureBox
            // 
            this.TrackingPictureBox.Location = new System.Drawing.Point(0, 0);
            this.TrackingPictureBox.Name = "TrackingPictureBox";
            this.TrackingPictureBox.Size = new System.Drawing.Size(500, 354);
            this.TrackingPictureBox.TabIndex = 8;
            this.TrackingPictureBox.TabStop = false;
            this.TrackingPictureBox.Click += new System.EventHandler(this.TrackingPictureBox_Click);
            // 
            // ImageTracking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 355);
            this.Controls.Add(this.TrackingPictureBox);
            this.Name = "ImageTracking";
            this.Text = "ImageTracking";
            ((System.ComponentModel.ISupportInitialize)(this.TrackingPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox TrackingPictureBox;
    }
}