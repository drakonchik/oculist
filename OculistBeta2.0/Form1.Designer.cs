﻿namespace OculistBeta2._0
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inspectorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textPointDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagePointDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.Configuration = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.timerDelayTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.timerDelaySlider = new System.Windows.Forms.TrackBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonRemoveText = new System.Windows.Forms.Button();
            this.buttonAddText = new System.Windows.Forms.Button();
            this.buttonRemoveImage = new System.Windows.Forms.Button();
            this.buttonAddImage = new System.Windows.Forms.Button();
            this.comboBoxTextFiles = new System.Windows.Forms.ComboBox();
            this.comboBoxImageFiles = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxLocalScreenPosition = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDownRadius = new System.Windows.Forms.NumericUpDown();
            this.CheckboxAskQuestionsAfterSession = new System.Windows.Forms.CheckBox();
            this.checkBoxDisplayReadjustedData = new System.Windows.Forms.CheckBox();
            this.checkboxRecordRightEye = new System.Windows.Forms.CheckBox();
            this.checkboxRecordLeftEyeData = new System.Windows.Forms.CheckBox();
            this.checkBoxDevelopmentMode = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.leftEyePupilDiameterTextBox = new System.Windows.Forms.TextBox();
            this.rightEyePupilDiameterTextBox = new System.Windows.Forms.TextBox();
            this.rightHeadEyeZTextBox = new System.Windows.Forms.TextBox();
            this.rightHeadEyeYTextBox = new System.Windows.Forms.TextBox();
            this.rightHeadEyeXTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.leftHeadEyeZTextBox = new System.Windows.Forms.TextBox();
            this.leftHeadEyeYTextBox = new System.Windows.Forms.TextBox();
            this.leftHeadEyeXTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bothEyesXTextBox = new System.Windows.Forms.TextBox();
            this.bothEyesYTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rightEyeXTextBox = new System.Windows.Forms.TextBox();
            this.rightEyeYTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.leftEyeXTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.leftEyeYTextBox = new System.Windows.Forms.TextBox();
            this.TextEyeTracking = new System.Windows.Forms.TabPage();
            this.textYAxis = new System.Windows.Forms.Label();
            this.textXAxis = new System.Windows.Forms.Label();
            this.TabTextControler = new System.Windows.Forms.TabControl();
            this.tabTextDevelopmentMode = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonSetTextPoint = new System.Windows.Forms.Button();
            this.textDevPointNumber = new System.Windows.Forms.NumericUpDown();
            this.textDevPosY = new System.Windows.Forms.NumericUpDown();
            this.textDevPosX = new System.Windows.Forms.NumericUpDown();
            this.textDevelopmentListBox = new System.Windows.Forms.ListBox();
            this.tabTextTrackingMode = new System.Windows.Forms.TabPage();
            this.boxTextExperimentControls = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.pauseUnpauseTextSession = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.butonShowTextTrackingWindow = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.buttonRequestTextPermission = new System.Windows.Forms.Button();
            this.textExperimentListBox = new System.Windows.Forms.ListBox();
            this.controlTextBox = new System.Windows.Forms.RichTextBox();
            this.ImageEyeTracking = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.imageDevelopmentTextBox = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.buttonSetImagePoint = new System.Windows.Forms.Button();
            this.imageDevPointNumber = new System.Windows.Forms.NumericUpDown();
            this.imageDevPosY = new System.Windows.Forms.NumericUpDown();
            this.imageDevPosX = new System.Windows.Forms.NumericUpDown();
            this.imageDevelopmentListBox = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.boxImageExperimentControls = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.pauseUnpauseImageSession = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.butonShowImageTrackingWindow = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.buttonRequestImagePermission = new System.Windows.Forms.Button();
            this.imageExperimentListBox = new System.Windows.Forms.ListBox();
            this.imageYAxis = new System.Windows.Forms.Label();
            this.imageXAxis = new System.Windows.Forms.Label();
            this.controlPictureBox = new System.Windows.Forms.PictureBox();
            this.EyeTrackerCorrection = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.numericUpDownScreenEyeTrackingYOffset = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownScreenEyeTrackingXOffset = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.correctionYAxis = new System.Windows.Forms.Label();
            this.correctionXAxis = new System.Windows.Forms.Label();
            this.QuestionTab = new System.Windows.Forms.TabPage();
            this.questionYAxis = new System.Windows.Forms.Label();
            this.questionXAxis = new System.Windows.Forms.Label();
            this.buttonSetQuestion = new System.Windows.Forms.Button();
            this.listboxTextOrImagePoints = new System.Windows.Forms.ListBox();
            this.checkBoxAskQuestionIfPointSeen = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxQuestions = new System.Windows.Forms.RichTextBox();
            this.pictureBoxQuestionSetup = new System.Windows.Forms.PictureBox();
            this.questionTextBox = new System.Windows.Forms.TextBox();
            this.radioButtonTextQuestions = new System.Windows.Forms.RadioButton();
            this.radioButtonImagesQuestions = new System.Windows.Forms.RadioButton();
            this.imagesOrTextListBox = new System.Windows.Forms.ListBox();
            this.MessageBox = new System.Windows.Forms.RichTextBox();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.MainTabControl.SuspendLayout();
            this.Configuration.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timerDelaySlider)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadius)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.TextEyeTracking.SuspendLayout();
            this.TabTextControler.SuspendLayout();
            this.tabTextDevelopmentMode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textDevPointNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDevPosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDevPosX)).BeginInit();
            this.tabTextTrackingMode.SuspendLayout();
            this.boxTextExperimentControls.SuspendLayout();
            this.ImageEyeTracking.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.imageDevelopmentTextBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageDevPointNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDevPosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDevPosX)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.boxImageExperimentControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlPictureBox)).BeginInit();
            this.EyeTrackerCorrection.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScreenEyeTrackingYOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScreenEyeTrackingXOffset)).BeginInit();
            this.QuestionTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQuestionSetup)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(944, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.openProjectToolStripMenuItem,
            this.saveProjectToolStripMenuItem,
            this.closeProjectToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.newProjectToolStripMenuItem.Text = "New Project";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.NewProjectToolStripMenuItem_Click);
            // 
            // openProjectToolStripMenuItem
            // 
            this.openProjectToolStripMenuItem.Name = "openProjectToolStripMenuItem";
            this.openProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.openProjectToolStripMenuItem.Text = "Open Project";
            this.openProjectToolStripMenuItem.Click += new System.EventHandler(this.OpenProjectToolStripMenuItem_Click);
            // 
            // saveProjectToolStripMenuItem
            // 
            this.saveProjectToolStripMenuItem.Name = "saveProjectToolStripMenuItem";
            this.saveProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.saveProjectToolStripMenuItem.Text = "Save Project";
            this.saveProjectToolStripMenuItem.Click += new System.EventHandler(this.SaveProjectToolStripMenuItem_Click);
            // 
            // closeProjectToolStripMenuItem
            // 
            this.closeProjectToolStripMenuItem.Name = "closeProjectToolStripMenuItem";
            this.closeProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.closeProjectToolStripMenuItem.Text = "Close Project";
            this.closeProjectToolStripMenuItem.Click += new System.EventHandler(this.CloseProjectToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inspectorToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // inspectorToolStripMenuItem
            // 
            this.inspectorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.textPointDataToolStripMenuItem,
            this.imagePointDataToolStripMenuItem});
            this.inspectorToolStripMenuItem.Name = "inspectorToolStripMenuItem";
            this.inspectorToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.inspectorToolStripMenuItem.Text = "Inspector";
            // 
            // textPointDataToolStripMenuItem
            // 
            this.textPointDataToolStripMenuItem.Name = "textPointDataToolStripMenuItem";
            this.textPointDataToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.textPointDataToolStripMenuItem.Text = "TextPointData";
            // 
            // imagePointDataToolStripMenuItem
            // 
            this.imagePointDataToolStripMenuItem.Name = "imagePointDataToolStripMenuItem";
            this.imagePointDataToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.imagePointDataToolStripMenuItem.Text = "ImagePointData";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 519);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(944, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(79, 17);
            this.toolStripStatusLabel1.Text = "Status update";
            // 
            // MainTabControl
            // 
            this.MainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTabControl.Controls.Add(this.Configuration);
            this.MainTabControl.Controls.Add(this.TextEyeTracking);
            this.MainTabControl.Controls.Add(this.ImageEyeTracking);
            this.MainTabControl.Controls.Add(this.EyeTrackerCorrection);
            this.MainTabControl.Controls.Add(this.QuestionTab);
            this.MainTabControl.Enabled = false;
            this.MainTabControl.Location = new System.Drawing.Point(13, 27);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(919, 397);
            this.MainTabControl.TabIndex = 2;
            // 
            // Configuration
            // 
            this.Configuration.Controls.Add(this.groupBox4);
            this.Configuration.Controls.Add(this.groupBox3);
            this.Configuration.Controls.Add(this.groupBox2);
            this.Configuration.Controls.Add(this.groupBox1);
            this.Configuration.Location = new System.Drawing.Point(4, 22);
            this.Configuration.Name = "Configuration";
            this.Configuration.Padding = new System.Windows.Forms.Padding(3);
            this.Configuration.Size = new System.Drawing.Size(911, 371);
            this.Configuration.TabIndex = 0;
            this.Configuration.Text = "Configuration";
            this.Configuration.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.timerDelayTextBox);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.timerDelaySlider);
            this.groupBox4.Location = new System.Drawing.Point(663, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(242, 265);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "System Configuration";
            // 
            // timerDelayTextBox
            // 
            this.timerDelayTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.timerDelayTextBox.Location = new System.Drawing.Point(104, 36);
            this.timerDelayTextBox.Name = "timerDelayTextBox";
            this.timerDelayTextBox.Size = new System.Drawing.Size(30, 20);
            this.timerDelayTextBox.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 39);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Timer Delay Slider";
            // 
            // timerDelaySlider
            // 
            this.timerDelaySlider.Location = new System.Drawing.Point(6, 55);
            this.timerDelaySlider.Maximum = 60;
            this.timerDelaySlider.Minimum = 1;
            this.timerDelaySlider.Name = "timerDelaySlider";
            this.timerDelaySlider.Size = new System.Drawing.Size(229, 45);
            this.timerDelaySlider.TabIndex = 0;
            this.timerDelaySlider.Value = 1;
            this.timerDelaySlider.Scroll += new System.EventHandler(this.TimerDelaySlider_Scroll);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.buttonRemoveText);
            this.groupBox3.Controls.Add(this.buttonAddText);
            this.groupBox3.Controls.Add(this.buttonRemoveImage);
            this.groupBox3.Controls.Add(this.buttonAddImage);
            this.groupBox3.Controls.Add(this.comboBoxTextFiles);
            th﻿namespace OculistBeta2._0
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inspectorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textPointDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagePointDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.Configuration = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.timerDelayTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.timerDelaySlider = new System.Windows.Forms.TrackBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonRemoveText = new System.Windows.Forms.Button();
            this.buttonAddText = new System.Windows.Forms.Button();
            this.buttonRemoveImage = new System.Windows.Forms.Button();
            this.buttonAddImage = new System.Windows.Forms.Button();
            this.comboBoxTextFiles = new System.Windows.Forms.ComboBox();
            this.comboBoxImageFiles = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxLocalScreenPosition = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDownRadius = new System.Windows.Forms.NumericUpDown();
            this.CheckboxAskQuestionsAfterSession = new System.Windows.Forms.CheckBox();
            this.checkBoxDisplayReadjustedData = new System.Windows.Forms.CheckBox();
            this.checkboxRecordRightEye = new System.Windows.Forms.CheckBox();
            this.checkboxRecordLeftEyeData = new System.Windows.Forms.CheckBox();
            this.checkBoxDevelopmentMode = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.leftEyePupilDiameterTextBox = new System.Windows.Forms.TextBox();
            this.rightEyePupilDiameterTextBox = new System.Windows.Forms.TextBox();
            this.rightHeadEyeZTextBox = new System.Windows.Forms.TextBox();
            this.rightHeadEyeYTextBox = new System.Windows.Forms.TextBox();
            this.rightHeadEyeXTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.leftHeadEyeZTextBox = new System.Windows.Forms.TextBox();
            this.leftHeadEyeYTextBox = new System.Windows.Forms.TextBox();
            this.leftHeadEyeXTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bothEyesXTextBox = new System.Windows.Forms.TextBox();
            this.bothEyesYTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rightEyeXTextBox = new System.Windows.Forms.TextBox();
            this.rightEyeYTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.leftEyeXTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.leftEyeYTextBox = new System.Windows.Forms.TextBox();
            this.TextEyeTracking = new System.Windows.Forms.TabPage();
            this.textYAxis = new System.Windows.Forms.Label();
            this.textXAxis = new System.Windows.Forms.Label();
            this.TabTextControler = new System.Windows.Forms.TabControl();
            this.tabTextDevelopmentMode = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonSetTextPoint = new System.Windows.Forms.Button();
            this.textDevPointNumber = new System.Windows.Forms.NumericUpDown();
            this.textDevPosY = new System.Windows.Forms.NumericUpDown();
            this.textDevPosX = new System.Windows.Forms.NumericUpDown();
            this.textDevelopmentListBox = new System.Windows.Forms.ListBox();
            this.tabTextTrackingMode = new System.Windows.Forms.TabPage();
            this.boxTextExperimentControls = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.pauseUnpauseTextSession = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.butonShowTextTrackingWindow = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.buttonRequestTextPermission = new System.Windows.Forms.Button();
            this.textExperimentListBox = new System.Windows.Forms.ListBox();
            this.controlTextBox = new System.Windows.Forms.RichTextBox();
            this.ImageEyeTracking = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.imageDevelopmentTextBox = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.buttonSetImagePoint = new System.Windows.Forms.Button();
            this.imageDevPointNumber = new System.Windows.Forms.NumericUpDown();
            this.imageDevPosY = new System.Windows.Forms.NumericUpDown();
            this.imageDevPosX = new System.Windows.Forms.NumericUpDown();
            this.imageDevelopmentListBox = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.boxImageExperimentControls = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.pauseUnpauseImageSession = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.butonShowImageTrackingWindow = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.buttonRequestImagePermission = new System.Windows.Forms.Button();
            this.imageExperimentListBox = new System.Windows.Forms.ListBox();
            this.imageYAxis = new System.Windows.Forms.Label();
            this.imageXAxis = new System.Windows.Forms.Label();
            this.controlPictureBox = new System.Windows.Forms.PictureBox();
            this.EyeTrackerCorrection = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.numericUpDownScreenEyeTrackingYOffset = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownScreenEyeTrackingXOffset = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.correctionYAxis = new System.Windows.Forms.Label();
            this.correctionXAxis = new System.Windows.Forms.Label();
            this.QuestionTab = new System.Windows.Forms.TabPage();
            this.questionYAxis = new System.Windows.Forms.Label();
            this.questionXAxis = new System.Windows.Forms.Label();
            this.buttonSetQuestion = new System.Windows.Forms.Button();
            this.listboxTextOrImagePoints = new System.Windows.Forms.ListBox();
            this.checkBoxAskQuestionIfPointSeen = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxQuestions = new System.Windows.Forms.RichTextBox();
            this.pictureBoxQuestionSetup = new System.Windows.Forms.PictureBox();
            this.questionTextBox = new System.Windows.Forms.TextBox();
            this.radioButtonTextQuestions = new System.Windows.Forms.RadioButton();
            this.radioButtonImagesQuestions = new System.Windows.Forms.RadioButton();
            this.imagesOrTextListBox = new System.Windows.Forms.ListBox();
            this.MessageBox = new System.Windows.Forms.RichTextBox();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.MainTabControl.SuspendLayout();
            this.Configuration.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timerDelaySlider)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadius)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.TextEyeTracking.SuspendLayout();
            this.TabTextControler.SuspendLayout();
            this.tabTextDevelopmentMode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textDevPointNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDevPosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDevPosX)).BeginInit();
            this.tabTextTrackingMode.SuspendLayout();
            this.boxTextExperimentControls.SuspendLayout();
            this.ImageEyeTracking.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.imageDevelopmentTextBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageDevPointNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDevPosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDevPosX)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.boxImageExperimentControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlPictureBox)).BeginInit();
            this.EyeTrackerCorrection.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScreenEyeTrackingYOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScreenEyeTrackingXOffset)).BeginInit();
            this.QuestionTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQuestionSetup)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(944, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.openProjectToolStripMenuItem,
            this.saveProjectToolStripMenuItem,
            this.closeProjectToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.newProjectToolStripMenuItem.Text = "New Project";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.NewProjectToolStripMenuItem_Click);
            // 
            // openProjectToolStripMenuItem
            // 
            this.openProjectToolStripMenuItem.Name = "openProjectToolStripMenuItem";
            this.openProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.openProjectToolStripMenuItem.Text = "Open Project";
            this.openProjectToolStripMenuItem.Click += new System.EventHandler(this.OpenProjectToolStripMenuItem_Click);
            // 
            // saveProjectToolStripMenuItem
            // 
            this.saveProjectToolStripMenuItem.Name = "saveProjectToolStripMenuItem";
            this.saveProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.saveProjectToolStripMenuItem.Text = "Save Project";
            this.saveProjectToolStripMenuItem.Click += new System.EventHandler(this.SaveProjectToolStripMenuItem_Click);
            // 
            // closeProjectToolStripMenuItem
            // 
            this.closeProjectToolStripMenuItem.Name = "closeProjectToolStripMenuItem";
            this.closeProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.closeProjectToolStripMenuItem.Text = "Close Project";
            this.closeProjectToolStripMenuItem.Click += new System.EventHandler(this.CloseProjectToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inspectorToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // inspectorToolStripMenuItem
            // 
            this.inspectorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.textPointDataToolStripMenuItem,
            this.imagePointDataToolStripMenuItem});
            this.inspectorToolStripMenuItem.Name = "inspectorToolStripMenuItem";
            this.inspectorToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.inspectorToolStripMenuItem.Text = "Inspector";
            // 
            // textPointDataToolStripMenuItem
            // 
            this.textPointDataToolStripMenuItem.Name = "textPointDataToolStripMenuItem";
            this.textPointDataToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.textPointDataToolStripMenuItem.Text = "TextPointData";
            // 
            // imagePointDataToolStripMenuItem
            // 
            this.imagePointDataToolStripMenuItem.Name = "imagePointDataToolStripMenuItem";
            this.imagePointDataToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.imagePointDataToolStripMenuItem.Text = "ImagePointData";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 519);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(944, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(79, 17);
            this.toolStripStatusLabel1.Text = "Status update";
            // 
            // MainTabControl
            // 
            this.MainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTabControl.Controls.Add(this.Configuration);
            this.MainTabControl.Controls.Add(this.TextEyeTracking);
            this.MainTabControl.Controls.Add(this.ImageEyeTracking);
            this.MainTabControl.Controls.Add(this.EyeTrackerCorrection);
            this.MainTabControl.Controls.Add(this.QuestionTab);
            this.MainTabControl.Enabled = false;
            this.MainTabControl.Location = new System.Drawing.Point(13, 27);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(919, 397);
            this.MainTabControl.TabIndex = 2;
            // 
            // Configuration
            // 
            this.Configuration.Controls.Add(this.groupBox4);
            this.Configuration.Controls.Add(this.groupBox3);
            this.Configuration.Controls.Add(this.groupBox2);
            this.Configuration.Controls.Add(this.groupBox1);
            this.Configuration.Location = new System.Drawing.Point(4, 22);
            this.Configuration.Name = "Configuration";
            this.Configuration.Padding = new System.Windows.Forms.Padding(3);
            this.Configuration.Size = new System.Drawing.Size(911, 371);
            this.Configuration.TabIndex = 0;
            this.Configuration.Text = "Configuration";
            this.Configuration.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.timerDelayTextBox);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.timerDelaySlider);
            this.groupBox4.Location = new System.Drawing.Point(663, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(242, 265);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "System Configuration";
            // 
            // timerDelayTextBox
            // 
            this.timerDelayTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.timerDelayTextBox.Location = new System.Drawing.Point(104, 36);
            this.timerDelayTextBox.Name = "timerDelayTextBox";
            this.timerDelayTextBox.Size = new System.Drawing.Size(30, 20);
            this.timerDelayTextBox.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 39);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Timer Delay Slider";
            // 
            // timerDelaySlider
            // 
            this.timerDelaySlider.Location = new System.Drawing.Point(6, 55);
            this.timerDelaySlider.Maximum = 60;
            this.timerDelaySlider.Minimum = 1;
            this.timerDelaySlider.Name = "timerDelaySlider";
            this.timerDelaySlider.Size = new System.Drawing.Size(229, 45);
            this.timerDelaySlider.TabIndex = 0;
            this.timerDelaySlider.Value = 1;
            this.timerDelaySlider.Scroll += new System.EventHandler(this.TimerDelaySlider_Scroll);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.buttonRemoveText);
            this.groupBox3.Controls.Add(this.buttonAddText);
            this.groupBox3.Controls.Add(this.buttonRemoveImage);
            this.groupBox3.Controls.Add(this.buttonAddImage);
            this.groupBox3.Controls.Add(this.comboBoxTextFiles);
            this.groupBox3.Controls.Add(this.comboBoxImageFiles);
            this.groupBox3.Location = new System.Drawing.Point(354, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(302, 266);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "File Management";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Text Files (.txt)";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Image Files (.png)";
            // 
            // buttonRemoveText
            // 
            this.buttonRemoveText.Location = new System.Drawing.Point(215, 107);
            this.buttonRemoveText.Name = "buttonRemoveText";
            this.buttonRemoveText.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveText.TabIndex = 5;
            this.buttonRemoveText.Text = "Remove";
            this.buttonRemoveText.UseVisualStyleBackColor = true;
            this.buttonRemoveText.Click += new System.EventHandler(this.ButtonRemoveText_Click);
            // 
            // buttonAddText
            // 
            this.buttonAddText.Location = new System.Drawing.Point(134, 107);
            this.buttonAddText.Name = "buttonAddText";
            this.buttonAddText.Size = new System.Drawing.Size(75, 23);
            this.buttonAddText.TabIndex = 4;
            this.buttonAddText.Text = "Add";
            this.buttonAddText.UseVisualStyleBackColor = true;
            this.buttonAddText.Click += new System.EventHandler(this.ButtonAddText_Click);
            // 
            // buttonRemoveImage
            // 
            this.buttonRemoveImage.Location = new System.Drawing.Point(215, 61);
            this.buttonRemoveImage.Name = "buttonRemoveImage";
            this.buttonRemoveImage.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveImage.TabIndex = 3;
            this.buttonRemoveImage.Text = "Remove";
            this.buttonRemoveImage.UseVisualStyleBackColor = true;
            this.buttonRemoveImage.Click += new System.EventHandler(this.ButtonRemoveImage_Click);
            // 
            // buttonAddImage
            // 
            this.buttonAddImage.Location = new System.Drawing.Point(134, 61);
            this.buttonAddImage.Name = "buttonAddImage";
            this.buttonAddImage.Size = new System.Drawing.Size(75, 23);
            this.buttonAddImage.TabIndex = 2;
            this.buttonAddImage.Text = "Add";
            this.buttonAddImage.UseVisualStyleBackColor = true;
            this.buttonAddImage.Click += new System.EventHandler(this.ButtonAddImage_Click);
            // 
            // comboBoxTextFiles
            // 
            this.comboBoxTextFiles.FormattingEnabled = true;
            this.comboBoxTextFiles.Location = new System.Drawing.Point(6, 107);
            this.comboBoxTextFiles.Name = "comboBoxTextFiles";
            this.comboBoxTextFiles.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTextFiles.TabIndex = 1;
            // 
            // comboBoxImageFiles
            // 
            this.comboBoxImageFiles.FormattingEnabled = true;
            this.comboBoxImageFiles.Location = new System.Drawing.Point(6, 61);
            this.comboBoxImageFiles.Name = "comboBoxImageFiles";
            this.comboBoxImageFiles.Size = new System.Drawing.Size(121, 21);
            this.comboBoxImageFiles.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxLocalScreenPosition);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.numericUpDownRadius);
            this.groupBox2.Controls.Add(this.CheckboxAskQuestionsAfterSession);
            this.groupBox2.Controls.Add(this.checkBoxDisplayReadjustedData);
            this.groupBox2.Controls.Add(this.checkboxRecordRightEye);
            this.groupBox2.Controls.Add(this.checkboxRecordLeftEyeData);
            this.groupBox2.Controls.Add(this.checkBoxDevelopmentMode);
            this.groupBox2.Location = new System.Drawing.Point(177, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(171, 266);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Configuration";
            // 
            // checkBoxLocalScreenPosition
            // 
            this.checkBoxLocalScreenPosition.AutoSize = true;
            this.checkBoxLocalScreenPosition.Location = new System.Drawing.Point(7, 40);
            this.checkBoxLocalScreenPosition.Name = "checkBoxLocalScreenPosition";
            this.checkBoxLocalScreenPosition.Size = new System.Drawing.Size(92, 17);
            this.checkBoxLocalScreenPosition.TabIndex = 8;
            this.checkBoxLocalScreenPosition.Text = "Local Position";
            this.checkBoxLocalScreenPosition.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(52, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Radius";
            // 
            // numericUpDownRadius
            // 
            this.numericUpDownRadius.Location = new System.Drawing.Point(7, 154);
            this.numericUpDownRadius.Name = "numericUpDownRadius";
            this.numericUpDownRadius.Size = new System.Drawing.Size(39, 20);
            this.numericUpDownRadius.TabIndex = 6;
            // 
            // CheckboxAskQuestionsAfterSession
            // 
            this.CheckboxAskQuestionsAfterSession.AutoSize = true;
            this.CheckboxAskQuestionsAfterSession.Location = new System.Drawing.Point(7, 131);
            this.CheckboxAskQuestionsAfterSession.Name = "CheckboxAskQuestionsAfterSession";
            this.CheckboxAskQuestionsAfterSession.Size = new System.Drawing.Size(73, 17);
            this.CheckboxAskQuestionsAfterSession.TabIndex = 4;
            this.CheckboxAskQuestionsAfterSession.Text = "Questions";
            this.CheckboxAskQuestionsAfterSession.UseVisualStyleBackColor = true;
            // 
            // checkBoxDisplayReadjustedData
            // 
            this.checkBoxDisplayReadjustedData.AutoSize = true;
            this.checkBoxDisplayReadjustedData.Location = new System.Drawing.Point(7, 108);
            this.checkBoxDisplayReadjustedData.Name = "checkBoxDisplayReadjustedData";
            this.checkBoxDisplayReadjustedData.Size = new System.Drawing.Size(127, 17);
            this.checkBoxDisplayReadjustedData.TabIndex = 3;
            this.checkBoxDisplayReadjustedData.Text = "Readjusted Eye Data";
            this.checkBoxDisplayReadjustedData.UseVisualStyleBackColor = true;
            // 
            // checkboxRecordRightEye
            // 
            this.checkboxRecordRightEye.AutoSize = true;
            this.checkboxRecordRightEye.Location = new System.Drawing.Point(7, 86);
            this.checkboxRecordRightEye.Name = "checkboxRecordRightEye";
            this.checkboxRecordRightEye.Size = new System.Drawing.Size(110, 17);
            this.checkboxRecordRightEye.TabIndex = 2;
            this.checkboxRecordRightEye.Text = "Record Right Eye";
            this.checkboxRecordRightEye.UseVisualStyleBackColor = true;
            // 
            // checkboxRecordLeftEyeData
            // 
            this.checkboxRecordLeftEyeData.AutoSize = true;
            this.checkboxRecordLeftEyeData.Location = new System.Drawing.Point(7, 63);
            this.checkboxRecordLeftEyeData.Name = "checkboxRecordLeftEyeData";
            this.checkboxRecordLeftEyeData.Size = new System.Drawing.Size(103, 17);
            this.checkboxRecordLeftEyeData.TabIndex = 1;
            this.checkboxRecordLeftEyeData.Text = "Record Left Eye";
            this.checkboxRecordLeftEyeData.UseVisualStyleBackColor = true;
            // 
            // checkBoxDevelopmentMode
            // 
            this.checkBoxDevelopmentMode.AutoSize = true;
            this.checkBoxDevelopmentMode.Location = new System.Drawing.Point(7, 19);
            this.checkBoxDevelopmentMode.Name = "checkBoxDevelopmentMode";
            this.checkBoxDevelopmentMode.Size = new System.Drawing.Size(119, 17);
            this.checkBoxDevelopmentMode.TabIndex = 0;
            this.checkBoxDevelopmentMode.Text = "Development Mode";
            this.checkBoxDevelopmentMode.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.leftEyePupilDiameterTextBox);
            this.groupBox1.Controls.Add(this.rightEyePupilDiameterTextBox);
            this.groupBox1.Controls.Add(this.rightHeadEyeZTextBox);
            this.groupBox1.Controls.Add(this.rightHeadEyeYTextBox);
            this.groupBox1.Controls.Add(this.rightHeadEyeXTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.leftHeadEyeZTextBox);
            this.groupBox1.Controls.Add(this.leftHeadEyeYTextBox);
            this.groupBox1.Controls.Add(this.leftHeadEyeXTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.bothEyesXTextBox);
            this.groupBox1.Controls.Add(this.bothEyesYTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.rightEyeXTextBox);
            this.groupBox1.Controls.Add(this.rightEyeYTextBox);
            this.groupBox1.Controls.Add(this.NameTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.leftEyeXTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.leftEyeYTextBox);
            this.groupBox1.Location = new System.Drawing.Point(0, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(171, 266);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General Data";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Radius L R";
            // 
            // leftEyePupilDiameterTextBox
            // 
            this.leftEyePupilDiameterTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftEyePupilDiameterTextBox.Location = new System.Drawing.Point(65, 175);
            this.leftEyePupilDiameterTextBox.Name = "leftEyePupilDiameterTextBox";
            this.leftEyePupilDiameterTextBox.Size = new System.Drawing.Size(48, 20);
            this.leftEyePupilDiameterTextBox.TabIndex = 19;
            // 
            // rightEyePupilDiameterTextBox
            // 
            this.rightEyePupilDiameterTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightEyePupilDiameterTextBox.Location = new System.Drawing.Point(117, 175);
            this.rightEyePupilDiameterTextBox.Name = "rightEyePupilDiameterTextBox";
            this.rightEyePupilDiameterTextBox.Size = new System.Drawing.Size(48, 20);
            this.rightEyePupilDiameterTextBox.TabIndex = 20;
            // 
            // rightHeadEyeZTextBox
            // 
            this.rightHeadEyeZTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightHeadEyeZTextBox.Location = new System.Drawing.Point(135, 149);
            this.rightHeadEyeZTextBox.Name = "rightHeadEyeZTextBox";
            this.rightHeadEyeZTextBox.Size = new System.Drawing.Size(30, 20);
            this.rightHeadEyeZTextBox.TabIndex = 18;
            // 
            // rightHeadEyeYTextBox
            // 
            this.rightHeadEyeYTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightHeadEyeYTextBox.Location = new System.Drawing.Point(101, 149);
            this.rightHeadEyeYTextBox.Name = "rightHeadEyeYTextBox";
            this.rightHeadEyeYTextBox.Size = new System.Drawing.Size(30, 20);
            this.rightHeadEyeYTextBox.TabIndex = 17;
            // 
            // rightHeadEyeXTextBox
            // 
            this.rightHeadEyeXTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightHeadEyeXTextBox.Location = new System.Drawing.Point(65, 149);
            this.rightHeadEyeXTextBox.Name = "rightHeadEyeXTextBox";
            this.rightHeadEyeXTextBox.Size = new System.Drawing.Size(30, 20);
            this.rightHeadEyeXTextBox.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "RHead XYZ";
            // 
            // leftHeadEyeZTextBox
            // 
            this.leftHeadEyeZTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftHeadEyeZTextBox.Location = new System.Drawing.Point(135, 123);
            this.leftHeadEyeZTextBox.Name = "leftHeadEyeZTextBox";
            this.leftHeadEyeZTextBox.Size = new System.Drawing.Size(30, 20);
            this.leftHeadEyeZTextBox.TabIndex = 14;
            // 
            // leftHeadEyeYTextBox
            // 
            this.leftHeadEyeYTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftHeadEyeYTextBox.Location = new System.Drawing.Point(101, 123);
            this.leftHeadEyeYTextBox.Name = "leftHeadEyeYTextBox";
            this.leftHeadEyeYTextBox.Size = new System.Drawing.Size(30, 20);
            this.leftHeadEyeYTextBox.TabIndex = 13;
            // 
            // leftHeadEyeXTextBox
            // 
            this.leftHeadEyeXTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftHeadEyeXTextBox.Location = new System.Drawing.Point(65, 123);
            this.leftHeadEyeXTextBox.Name = "leftHeadEyeXTextBox";
            this.leftHeadEyeXTextBox.Size = new System.Drawing.Size(30, 20);
            this.leftHeadEyeXTextBox.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "LHead XYZ";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Both X Y";
            // 
            // bothEyesXTextBox
            // 
            this.bothEyesXTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bothEyesXTextBox.Location = new System.Drawing.Point(65, 97);
            this.bothEyesXTextBox.Name = "bothEyesXTextBox";
            this.bothEyesXTextBox.Size = new System.Drawing.Size(48, 20);
            this.bothEyesXTextBox.TabIndex = 8;
            // 
            // bothEyesYTextBox
            // 
            this.bothEyesYTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bothEyesYTextBox.Location = new System.Drawing.Point(117, 97);
            this.bothEyesYTextBox.Name = "bothEyesYTextBox";
            this.bothEyesYTextBox.Size = new System.Drawing.Size(48, 20);
            this.bothEyesYTextBox.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Right X Y";
            // 
            // rightEyeXTextBox
            // 
            this.rightEyeXTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightEyeXTextBox.Location = new System.Drawing.Point(65, 71);
            this.rightEyeXTextBox.Name = "rightEyeXTextBox";
            this.rightEyeXTextBox.Size = new System.Drawing.Size(48, 20);
            this.rightEyeXTextBox.TabIndex = 5;
            // 
            // rightEyeYTextBox
            // 
            this.rightEyeYTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightEyeYTextBox.Location = new System.Drawing.Point(117, 71);
            this.rightEyeYTextBox.Name = "rightEyeYTextBox";
            this.rightEyeYTextBox.Size = new System.Drawing.Size(48, 20);
            this.rightEyeYTextBox.TabIndex = 6;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameTextBox.Location = new System.Drawing.Point(65, 19);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(100, 20);
            this.NameTextBox.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Left X Y";
            // 
            // leftEyeXTextBox
            // 
            this.leftEyeXTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftEyeXTextBox.Location = new System.Drawing.Point(65, 45);
            this.leftEyeXTextBox.Name = "leftEyeXTextBox";
            this.leftEyeXTextBox.Size = new System.Drawing.Size(48, 20);
            this.leftEyeXTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            // 
            // leftEyeYTextBox
            // 
            this.leftEyeYTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.leftEyeYTextBox.Location = new System.Drawing.Point(117, 45);
            this.leftEyeYTextBox.Name = "leftEyeYTextBox";
            this.leftEyeYTextBox.Size = new System.Drawing.Size(48, 20);
            this.leftEyeYTextBox.TabIndex = 2;
            // 
            // TextEyeTracking
            // 
            this.TextEyeTracking.Controls.Add(this.textYAxis);
            this.TextEyeTracking.Controls.Add(this.textXAxis);
            this.TextEyeTracking.Controls.Add(this.TabTextControler);
            this.TextEyeTracking.Controls.Add(this.controlTextBox);
            this.TextEyeTracking.Location = new System.Drawing.Point(4, 22);
            this.TextEyeTracking.Name = "TextEyeTracking";
            this.TextEyeTracking.Padding = new System.Windows.Forms.Padding(3);
            this.TextEyeTracking.Size = new System.Drawing.Size(911, 371);
            this.TextEyeTracking.TabIndex = 1;
            this.TextEyeTracking.Text = "Text Eye Tracking";
            this.TextEyeTracking.UseVisualStyleBackColor = true;
            // 
            // textYAxis
            // 
            this.textYAxis.BackColor = System.Drawing.Color.Black;
            this.textYAxis.Location = new System.Drawing.Point(6, 9);
            this.textYAxis.Name = "textYAxis";
            this.textYAxis.Size = new System.Drawing.Size(500, 3);
            this.textYAxis.TabIndex = 7;
            this.textYAxis.Text = "0";
            // 
            // textXAxis
            // 
            this.textXAxis.BackColor = System.Drawing.Color.Black;
            this.textXAxis.Location = new System.Drawing.Point(7, 7);
            this.textXAxis.Name = "textXAxis";
            this.textXAxis.Size = new System.Drawing.Size(3, 364);
            this.textXAxis.TabIndex = 6;
            this.textXAxis.Text = "0";
            // 
            // TabTextControler
            // 
            this.TabTextControler.Controls.Add(this.tabTextDevelopmentMode);
            this.TabTextControler.Controls.Add(this.tabTextTrackingMode);
            this.TabTextControler.Location = new System.Drawing.Point(513, 7);
            this.TabTextControler.Name = "TabTextControler";
            this.TabTextControler.SelectedIndex = 0;
            this.TabTextControler.Size = new System.Drawing.Size(395, 358);
            this.TabTextControler.TabIndex = 5;
            // 
            // tabTextDevelopmentMode
            // 
            this.tabTextDevelopmentMode.Controls.Add(this.label19);
            this.tabTextDevelopmentMode.Controls.Add(this.label18);
            this.tabTextDevelopmentMode.Controls.Add(this.label17);
            this.tabTextDevelopmentMode.Controls.Add(this.buttonSetTextPoint);
            this.tabTextDevelopmentMode.Controls.Add(this.textDevPointNumber);
            this.tabTextDevelopmentMode.Controls.Add(this.textDevPosY);
            this.tabTextDevelopmentMode.Controls.Add(this.textDevPosX);
            this.tabTextDevelopmentMode.Controls.Add(this.textDevelopmentListBox);
            this.tabTextDevelopmentMode.Location = new System.Drawing.Point(4, 22);
            this.tabTextDevelopmentMode.Name = "tabTextDevelopmentMode";
            this.tabTextDevelopmentMode.Padding = new System.Windows.Forms.Padding(3);
            this.tabTextDevelopmentMode.Size = new System.Drawing.Size(387, 332);
            this.tabTextDevelopmentMode.TabIndex = 0;
            this.tabTextDevelopmentMode.Text = "Development";
            this.tabTextDevelopmentMode.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(193, 43);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = "Pos Y";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(129, 43);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Pos X";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(180, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "Point Nr.";
            // 
            // buttonSetTextPoint
            // 
            this.buttonSetTextPoint.Location = new System.Drawing.Point(132, 86);
            this.buttonSetTextPoint.Name = "buttonSetTextPoint";
            this.buttonSetTextPoint.Size = new System.Drawing.Size(96, 36);
            this.buttonSetTextPoint.TabIndex = 4;
            this.buttonSetTextPoint.Text = "Set";
            this.buttonSetTextPoint.UseVisualStyleBackColor = true;
            this.buttonSetTextPoint.Click += new System.EventHandler(this.ButtonSetTextPoint_Click);
            // 
            // textDevPointNumber
            // 
            this.textDevPointNumber.Location = new System.Drawing.Point(132, 13);
            this.textDevPointNumber.Name = "textDevPointNumber";
            this.textDevPointNumber.Size = new System.Drawing.Size(41, 20);
            this.textDevPointNumber.TabIndex = 3;
            // 
            // textDevPosY
            // 
            this.textDevPosY.Location = new System.Drawing.Point(185, 59);
            this.textDevPosY.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.textDevPosY.Name = "textDevPosY";
            this.textDevPosY.Size = new System.Drawing.Size(47, 20);
            this.textDevPosY.TabIndex = 2;
            // 
            // textDevPosX
            // 
            this.textDevPosX.Location = new System.Drawing.Point(132, 59);
            this.textDevPosX.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.textDevPosX.Name = "textDevPosX";
            this.textDevPosX.Size = new System.Drawing.Size(47, 20);
            this.textDevPosX.TabIndex = 1;
            // 
            // textDevelopmentListBox
            // 
            this.textDevelopmentListBox.FormattingEnabled = true;
            this.textDevelopmentListBox.Location = new System.Drawing.Point(6, 13);
            this.textDevelopmentListBox.Name = "textDevelopmentListBox";
            this.textDevelopmentListBox.Size = new System.Drawing.Size(120, 303);
            this.textDevelopmentListBox.TabIndex = 0;
            // 
            // tabTextTrackingMode
            // 
            this.tabTextTrackingMode.Controls.Add(this.boxTextExperimentControls);
            this.tabTextTrackingMode.Controls.Add(this.label20);
            this.tabTextTrackingMode.Controls.Add(this.buttonRequestTextPermission);
            this.tabTextTrackingMode.Controls.Add(this.textExperimentListBox);
            this.tabTextTrackingMode.Location = new System.Drawing.Point(4, 22);
            this.tabTextTrackingMode.Name = "tabTextTrackingMode";
            this.tabTextTrackingMode.Padding = new System.Windows.Forms.Padding(3);
            this.tabTextTrackingMode.Size = new System.Drawing.Size(387, 332);
            this.tabTextTrackingMode.TabIndex = 1;
            this.tabTextTrackingMode.Text = "Tracking Experiment";
            this.tabTextTrackingMode.UseVisualStyleBackColor = true;
            // 
            // boxTextExperimentControls
            // 
            this.boxTextExperimentControls.Controls.Add(this.label29);
            this.boxTextExperimentControls.Controls.Add(this.pauseUnpauseTextSession);
            this.boxTextExperimentControls.Controls.Add(this.label21);
            this.boxTextExperimentControls.Controls.Add(this.butonShowTextTrackingWindow);
            this.boxTextExperimentControls.Location = new System.Drawing.Point(133, 49);
            this.boxTextExperimentControls.Name = "boxTextExperimentControls";
            this.boxTextExperimentControls.Size = new System.Drawing.Size(248, 267);
            this.boxTextExperimentControls.TabIndex = 4;
            this.boxTextExperimentControls.TabStop = false;
            this.boxTextExperimentControls.Text = "Experiment Controls";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(58, 53);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 13);
            this.label29.TabIndex = 7;
            this.label29.Text = "Session:";
            // 
            // pauseUnpauseTextSession
            // 
            this.pauseUnpauseTextSession.Location = new System.Drawing.Point(111, 48);
            this.pauseUnpauseTextSession.Name = "pauseUnpauseTextSession";
            this.pauseUnpauseTextSession.Size = new System.Drawing.Size(75, 23);
            this.pauseUnpauseTextSession.TabIndex = 6;
            this.pauseUnpauseTextSession.Text = "Pause";
            this.pauseUnpauseTextSession.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 24);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(94, 13);
            this.label21.TabIndex = 5;
            this.label21.Text = "Tracking Window:";
            // 
            // butonShowTextTrackingWindow
            // 
            this.butonShowTextTrackingWindow.Location = new System.Drawing.Point(111, 19);
            this.butonShowTextTrackingWindow.Name = "butonShowTextTrackingWindow";
            this.butonShowTextTrackingWindow.Size = new System.Drawing.Size(75, 23);
            this.butonShowTextTrackingWindow.TabIndex = 4;
            this.butonShowTextTrackingWindow.Text = "Show";
            this.butonShowTextTrackingWindow.UseVisualStyleBackColor = true;
            this.butonShowTextTrackingWindow.Click += new System.EventHandler(this.butonShowTextTrackingWindow_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(133, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "User consent:";
            // 
            // buttonRequestTextPermission
            // 
            this.buttonRequestTextPermission.Location = new System.Drawing.Point(212, 17);
            this.buttonRequestTextPermission.Name = "buttonRequestTextPermission";
            this.buttonRequestTextPermission.Size = new System.Drawing.Size(75, 23);
            this.buttonRequestTextPermission.TabIndex = 2;
            this.buttonRequestTextPermission.Text = "Request";
            this.buttonRequestTextPermission.UseVisualStyleBackColor = true;
            this.buttonRequestTextPermission.Click += new System.EventHandler(this.ButtonRequestTextPermission_Click);
            // 
            // textExperimentListBox
            // 
            this.textExperimentListBox.FormattingEnabled = true;
            this.textExperimentListBox.Location = new System.Drawing.Point(6, 13);
            this.textExperimentListBox.Name = "textExperimentListBox";
            this.textExperimentListBox.Size = new System.Drawing.Size(120, 303);
            this.textExperimentListBox.TabIndex = 1;
            // 
            // controlTextBox
            // 
            this.controlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controlTextBox.Location = new System.Drawing.Point(6, 6);
            this.controlTextBox.Name = "controlTextBox";
            this.controlTextBox.Size = new System.Drawing.Size(500, 360);
            this.controlTextBox.TabIndex = 4;
            this.controlTextBox.Text = "";
            // 
            // ImageEyeTracking
            // 
            this.ImageEyeTracking.Controls.Add(this.tabControl1);
            this.ImageEyeTracking.Controls.Add(this.imageYAxis);
            this.ImageEyeTracking.Controls.Add(this.imageXAxis);
            this.ImageEyeTracking.Controls.Add(this.controlPictureBox);
            this.ImageEyeTracking.Location = new System.Drawing.Point(4, 22);
            this.ImageEyeTracking.Name = "ImageEyeTracking";
            this.ImageEyeTracking.Padding = new System.Windows.Forms.Padding(3);
            this.ImageEyeTracking.Size = new System.Drawing.Size(911, 371);
            this.ImageEyeTracking.TabIndex = 2;
            this.ImageEyeTracking.Text = "Image Eye Tracking";
            this.ImageEyeTracking.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.imageDevelopmentTextBox);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(513, 7);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(395, 358);
            this.tabControl1.TabIndex = 10;
            // 
            // imageDevelopmentTextBox
            // 
            this.imageDevelopmentTextBox.Controls.Add(this.label22);
            this.imageDevelopmentTextBox.Controls.Add(this.label23);
            this.imageDevelopmentTextBox.Controls.Add(this.label24);
            this.imageDevelopmentTextBox.Controls.Add(this.buttonSetImagePoint);
            this.imageDevelopmentTextBox.Controls.Add(this.imageDevPointNumber);
            this.imageDevelopmentTextBox.Controls.Add(this.imageDevPosY);
            this.imageDevelopmentTextBox.Controls.Add(this.imageDevPosX);
            this.imageDevelopmentTextBox.Controls.Add(this.imageDevelopmentListBox);
            this.imageDevelopmentTextBox.Location = new System.Drawing.Point(4, 22);
            this.imageDevelopmentTextBox.Name = "imageDevelopmentTextBox";
            this.imageDevelopmentTextBox.Padding = new System.Windows.Forms.Padding(3);
            this.imageDevelopmentTextBox.Size = new System.Drawing.Size(387, 332);
            this.imageDevelopmentTextBox.TabIndex = 0;
            this.imageDevelopmentTextBox.Text = "Development";
            this.imageDevelopmentTextBox.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(193, 43);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "Pos Y";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(129, 43);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 6;
            this.label23.Text = "Pos X";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(180, 19);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 5;
            this.label24.Text = "Point Nr.";
            // 
            // buttonSetImagePoint
            // 
            this.buttonSetImagePoint.Location = new System.Drawing.Point(132, 86);
            this.buttonSetImagePoint.Name = "buttonSetImagePoint";
            this.buttonSetImagePoint.Size = new System.Drawing.Size(96, 36);
            this.buttonSetImagePoint.TabIndex = 4;
            this.buttonSetImagePoint.Text = "Set";
            this.buttonSetImagePoint.UseVisualStyleBackColor = true;
            this.buttonSetImagePoint.Click += new System.EventHandler(this.buttonSetImagePoint_Click);
            // 
            // imageDevPointNumber
            // 
            this.imageDevPointNumber.Location = new System.Drawing.Point(132, 13);
            this.imageDevPointNumber.Name = "imageDevPointNumber";
            this.imageDevPointNumber.Size = new System.Drawing.Size(41, 20);
            this.imageDevPointNumber.TabIndex = 3;
            // 
            // imageDevPosY
            // 
            this.imageDevPosY.Location = new System.Drawing.Point(185, 59);
            this.imageDevPosY.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.imageDevPosY.Name = "imageDevPosY";
            this.imageDevPosY.Size = new System.Drawing.Size(47, 20);
            this.imageDevPosY.TabIndex = 2;
            // 
            // imageDevPosX
            // 
            this.imageDevPosX.Location = new System.Drawing.Point(132, 59);
            this.imageDevPosX.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.imageDevPosX.Name = "imageDevPosX";
            this.imageDevPosX.Size = new System.Drawing.Size(47, 20);
            this.imageDevPosX.TabIndex = 1;
            // 
            // imageDevelopmentListBox
            // 
            this.imageDevelopmentListBox.FormattingEnabled = true;
            this.imageDevelopmentListBox.Location = new System.Drawing.Point(6, 13);
            this.imageDevelopmentListBox.Name = "imageDevelopmentListBox";
            this.imageDevelopmentListBox.Size = new System.Drawing.Size(120, 303);
            this.imageDevelopmentListBox.TabIndex = 0;
            this.imageDevelopmentListBox.SelectedIndexChanged += new System.EventHandler(this.ImageDevelopmentListBox_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.boxImageExperimentControls);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.buttonRequestImagePermission);
            this.tabPage2.Controls.Add(this.imageExperimentListBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(387, 332);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tracking Experiment";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // boxImageExperimentControls
            // 
            this.boxImageExperimentControls.Controls.Add(this.label30);
            this.boxImageExperimentControls.Controls.Add(this.pauseUnpauseImageSession);
            this.boxImageExperimentControls.Controls.Add(this.label25);
            this.boxImageExperimentControls.Controls.Add(this.butonShowImageTrackingWindow);
            this.boxImageExperimentControls.Location = new System.Drawing.Point(133, 49);
            this.boxImageExperimentControls.Name = "boxImageExperimentControls";
            this.boxImageExperimentControls.Size = new System.Drawing.Size(248, 267);
            this.boxImageExperimentControls.TabIndex = 4;
            this.boxImageExperimentControls.TabStop = false;
            this.boxImageExperimentControls.Text = "Experiment Controls";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(58, 53);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(47, 13);
            this.label30.TabIndex = 9;
            this.label30.Text = "Session:";
            // 
            // pauseUnpauseImageSession
            // 
            this.pauseUnpauseImageSession.Location = new System.Drawing.Point(111, 48);
            this.pauseUnpauseImageSession.Name = "pauseUnpauseImageSession";
            this.pauseUnpauseImageSession.Size = new System.Drawing.Size(75, 23);
            this.pauseUnpauseImageSession.TabIndex = 8;
            this.pauseUnpauseImageSession.Text = "Pause";
            this.pauseUnpauseImageSession.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(11, 24);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(94, 13);
            this.label25.TabIndex = 5;
            this.label25.Text = "Tracking Window:";
            // 
            // butonShowImageTrackingWindow
            // 
            this.butonShowImageTrackingWindow.Location = new System.Drawing.Point(111, 19);
            this.butonShowImageTrackingWindow.Name = "butonShowImageTrackingWindow";
            this.butonShowImageTrackingWindow.Size = new System.Drawing.Size(75, 23);
            this.butonShowImageTrackingWindow.TabIndex = 4;
            this.butonShowImageTrackingWindow.Text = "Show";
            this.butonShowImageTrackingWindow.UseVisualStyleBackColor = true;
            this.butonShowImageTrackingWindow.Click += new System.EventHandler(this.butonShowImageTrackingWindow_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(133, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 13);
            this.label26.TabIndex = 3;
            this.label26.Text = "User consent:";
            // 
            // buttonRequestImagePermission
            // 
            this.buttonRequestImagePermission.Location = new System.Drawing.Point(212, 17);
            this.buttonRequestImagePermission.Name = "buttonRequestImagePermission";
            this.buttonRequestImagePermission.Size = new System.Drawing.Size(75, 23);
            this.buttonRequestImagePermission.TabIndex = 2;
            this.buttonRequestImagePermission.Text = "Request";
            this.buttonRequestImagePermission.UseVisualStyleBackColor = true;
            this.buttonRequestImagePermission.Click += new System.EventHandler(this.ButtonRequestImagePermission_Click);
            // 
            // imageExperimentListBox
            // 
            this.imageExperimentListBox.FormattingEnabled = true;
            this.imageExperimentListBox.Location = new System.Drawing.Point(6, 13);
            this.imageExperimentListBox.Name = "imageExperimentListBox";
            this.imageExperimentListBox.Size = new System.Drawing.Size(120, 303);
            this.imageExperimentListBox.TabIndex = 1;
            this.imageExperimentListBox.SelectedIndexChanged += new System.EventHandler(this.imageExperimentListBox_SelectedIndexChanged);
            // 
            // imageYAxis
            // 
            this.imageYAxis.BackColor = System.Drawing.Color.Black;
            this.imageYAxis.Location = new System.Drawing.Point(5, 5);
            this.imageYAxis.Name = "imageYAxis";
            this.imageYAxis.Size = new System.Drawing.Size(500, 3);
            this.imageYAxis.TabIndex = 9;
            this.imageYAxis.Text = "0";
            // 
            // imageXAxis
            // 
            this.imageXAxis.BackColor = System.Drawing.Color.Black;
            this.imageXAxis.Location = new System.Drawing.Point(6, 3);
            this.imageXAxis.Name = "imageXAxis";
            this.imageXAxis.Size = new System.Drawing.Size(3, 364);
            this.imageXAxis.TabIndex = 8;
            this.imageXAxis.Text = "0";
            // 
            // controlPictureBox
            // 
            this.controlPictureBox.Location = new System.Drawing.Point(7, 7);
            this.controlPictureBox.Name = "controlPictureBox";
            this.controlPictureBox.Size = new System.Drawing.Size(500, 354);
            this.controlPictureBox.TabIndex = 7;
            this.controlPictureBox.TabStop = false;
            this.controlPictureBox.Click += new System.EventHandler(this.controlPictureBox_Click);
            // 
            // EyeTrackerCorrection
            // 
            this.EyeTrackerCorrection.Controls.Add(this.groupBox6);
            this.EyeTrackerCorrection.Controls.Add(this.panel1);
            this.EyeTrackerCorrection.Controls.Add(this.correctionYAxis);
            this.EyeTrackerCorrection.Controls.Add(this.correctionXAxis);
            this.EyeTrackerCorrection.Location = new System.Drawing.Point(4, 22);
            this.EyeTrackerCorrection.Name = "EyeTrackerCorrection";
            this.EyeTrackerCorrection.Padding = new System.Windows.Forms.Padding(3);
            this.EyeTrackerCorrection.Size = new System.Drawing.Size(911, 371);
            this.EyeTrackerCorrection.TabIndex = 3;
            this.EyeTrackerCorrection.Text = "Eye Tracker Correcting";
            this.EyeTrackerCorrection.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.numericUpDownScreenEyeTrackingYOffset);
            this.groupBox6.Controls.Add(this.numericUpDownScreenEyeTrackingXOffset);
            this.groupBox6.Location = new System.Drawing.Point(711, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(194, 359);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Tracking Correcting";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(90, 27);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(45, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Y-Offset";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(15, 27);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(45, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "X-Offset";
            // 
            // numericUpDownScreenEyeTrackingYOffset
            // 
            this.numericUpDownScreenEyeTrackingYOffset.Location = new System.Drawing.Point(93, 46);
            this.numericUpDownScreenEyeTrackingYOffset.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDownScreenEyeTrackingYOffset.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDownScreenEyeTrackingYOffset.Name = "numericUpDownScreenEyeTrackingYOffset";
            this.numericUpDownScreenEyeTrackingYOffset.Size = new System.Drawing.Size(72, 20);
            this.numericUpDownScreenEyeTrackingYOffset.TabIndex = 1;
            // 
            // numericUpDownScreenEyeTrackingXOffset
            // 
            this.numericUpDownScreenEyeTrackingXOffset.Location = new System.Drawing.Point(15, 46);
            this.numericUpDownScreenEyeTrackingXOffset.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDownScreenEyeTrackingXOffset.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.numericUpDownScreenEyeTrackingXOffset.Name = "numericUpDownScreenEyeTrackingXOffset";
            this.numericUpDownScreenEyeTrackingXOffset.Size = new System.Drawing.Size(72, 20);
            this.numericUpDownScreenEyeTrackingXOffset.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(426, 188);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(25, 25);
            this.panel1.TabIndex = 10;
            // 
            // correctionYAxis
            // 
            this.correctionYAxis.BackColor = System.Drawing.Color.Black;
            this.correctionYAxis.Location = new System.Drawing.Point(206, 0);
            this.correctionYAxis.Name = "correctionYAxis";
            this.correctionYAxis.Size = new System.Drawing.Size(500, 3);
            this.correctionYAxis.TabIndex = 9;
            this.correctionYAxis.Text = "0";
            // 
            // correctionXAxis
            // 
            this.correctionXAxis.BackColor = System.Drawing.Color.Black;
            this.correctionXAxis.Location = new System.Drawing.Point(206, 0);
            this.correctionXAxis.Name = "correctionXAxis";
            this.correctionXAxis.Size = new System.Drawing.Size(3, 373);
            this.correctionXAxis.TabIndex = 8;
            this.correctionXAxis.Text = "0";
            // 
            // QuestionTab
            // 
            this.QuestionTab.Controls.Add(this.questionYAxis);
            this.QuestionTab.Controls.Add(this.questionXAxis);
            this.QuestionTab.Controls.Add(this.buttonSetQuestion);
            this.QuestionTab.Controls.Add(this.listboxTextOrImagePoints);
            this.QuestionTab.Controls.Add(this.checkBoxAskQuestionIfPointSeen);
            this.QuestionTab.Controls.Add(this.label31);
            this.QuestionTab.Controls.Add(this.textBoxQuestions);
            this.QuestionTab.Controls.Add(this.pictureBoxQuestionSetup);
            this.QuestionTab.Controls.Add(this.questionTextBox);
            this.QuestionTab.Controls.Add(this.radioButtonTextQuestions);
            this.QuestionTab.Controls.Add(this.radioButtonImagesQuestions);
            this.QuestionTab.Controls.Add(this.imagesOrTextListBox);
            this.QuestionTab.Location = new System.Drawing.Point(4, 22);
            this.QuestionTab.Name = "QuestionTab";
            this.QuestionTab.Padding = new System.Windows.Forms.Padding(3);
            this.QuestionTab.Size = new System.Drawing.Size(911, 371);
            this.QuestionTab.TabIndex = 4;
            this.QuestionTab.Text = "Question Setup";
            this.QuestionTab.UseVisualStyleBackColor = true;
            // 
            // questionYAxis
            // 
            this.questionYAxis.BackColor = System.Drawing.Color.Black;
            this.questionYAxis.Location = new System.Drawing.Point(405, 6);
            this.questionYAxis.Name = "questionYAxis";
            this.questionYAxis.Size = new System.Drawing.Size(500, 3);
            this.questionYAxis.TabIndex = 15;
            this.questionYAxis.Text = "0";
            // 
            // questionXAxis
            // 
            this.questionXAxis.BackColor = System.Drawing.Color.Black;
            this.questionXAxis.Location = new System.Drawing.Point(406, 4);
            this.questionXAxis.Name = "questionXAxis";
            this.questionXAxis.Size = new System.Drawing.Size(3, 364);
            this.questionXAxis.TabIndex = 14;
            this.questionXAxis.Text = "0";
            // 
            // buttonSetQuestion
            // 
            this.buttonSetQuestion.Location = new System.Drawing.Point(272, 111);
            this.buttonSetQuestion.Name = "buttonSetQuestion";
            this.buttonSetQuestion.Size = new System.Drawing.Size(75, 23);
            this.buttonSetQuestion.TabIndex = 13;
            this.buttonSetQuestion.Text = "Set";
            this.buttonSetQuestion.UseVisualStyleBackColor = true;
            this.buttonSetQuestion.Click += new System.EventHandler(this.buttonSetQuestion_Click);
            // 
            // listboxTextOrImagePoints
            // 
            this.listboxTextOrImagePoints.FormattingEnabled = true;
            this.listboxTextOrImagePoints.Location = new System.Drawing.Point(132, 109);
            this.listboxTextOrImagePoints.Name = "listboxTextOrImagePoints";
            this.listboxTextOrImagePoints.Size = new System.Drawing.Size(120, 251);
            this.listboxTextOrImagePoints.TabIndex = 12;
            this.listboxTextOrImagePoints.SelectedIndexChanged += new System.EventHandler(this.ListboxTextOrImagePoints_SelectedIndexChanged);
            // 
            // checkBoxAskQuestionIfPointSeen
            // 
            this.checkBoxAskQuestionIfPointSeen.AutoSize = true;
            this.checkBoxAskQuestionIfPointSeen.Location = new System.Drawing.Point(272, 65);
            this.checkBoxAskQuestionIfPointSeen.Name = "checkBoxAskQuestionIfPointSeen";
            this.checkBoxAskQuestionIfPointSeen.Size = new System.Drawing.Size(128, 17);
            this.checkBoxAskQuestionIfPointSeen.TabIndex = 11;
            this.checkBoxAskQuestionIfPointSeen.Text = "Ask when point seen.";
            this.checkBoxAskQuestionIfPointSeen.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(132, 66);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(49, 13);
            this.label31.TabIndex = 10;
            this.label31.Text = "Question";
            // 
            // textBoxQuestions
            // 
            this.textBoxQuestions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxQuestions.Location = new System.Drawing.Point(407, 6);
            this.textBoxQuestions.Name = "textBoxQuestions";
            this.textBoxQuestions.Size = new System.Drawing.Size(500, 360);
            this.textBoxQuestions.TabIndex = 9;
            this.textBoxQuestions.Text = "";
            // 
            // pictureBoxQuestionSetup
            // 
            this.pictureBoxQuestionSetup.Location = new System.Drawing.Point(407, 6);
            this.pictureBoxQuestionSetup.Name = "pictureBoxQuestionSetup";
            this.pictureBoxQuestionSetup.Size = new System.Drawing.Size(500, 354);
            this.pictureBoxQuestionSetup.TabIndex = 8;
            this.pictureBoxQuestionSetup.TabStop = false;
            // 
            // questionTextBox
            // 
            this.questionTextBox.Location = new System.Drawing.Point(132, 85);
            this.questionTextBox.Name = "questionTextBox";
            this.questionTextBox.Size = new System.Drawing.Size(268, 20);
            this.questionTextBox.TabIndex = 5;
            // 
            // radioButtonTextQuestions
            // 
            this.radioButtonTextQuestions.AutoSize = true;
            this.radioButtonTextQuestions.Location = new System.Drawing.Point(133, 30);
            this.radioButtonTextQuestions.Name = "radioButtonTextQuestions";
            this.radioButtonTextQuestions.Size = new System.Drawing.Size(46, 17);
            this.radioButtonTextQuestions.TabIndex = 4;
            this.radioButtonTextQuestions.Text = "Text";
            this.radioButtonTextQuestions.UseVisualStyleBackColor = true;
            this.radioButtonTextQuestions.CheckedChanged += new System.EventHandler(this.RadioButtonTextQuestions_CheckedChanged);
            // 
            // radioButtonImagesQuestions
            // 
            this.radioButtonImagesQuestions.AutoSize = true;
            this.radioButtonImagesQuestions.Checked = true;
            this.radioButtonImagesQuestions.Location = new System.Drawing.Point(133, 7);
            this.radioButtonImagesQuestions.Name = "radioButtonImagesQuestions";
            this.radioButtonImagesQuestions.Size = new System.Drawing.Size(59, 17);
            this.radioButtonImagesQuestions.TabIndex = 3;
            this.radioButtonImagesQuestions.TabStop = true;
            this.radioButtonImagesQuestions.Text = "Images";
            this.radioButtonImagesQuestions.UseVisualStyleBackColor = true;
            this.radioButtonImagesQuestions.CheckedChanged += new System.EventHandler(this.RadioButtonImagesQuestions_CheckedChanged);
            // 
            // imagesOrTextListBox
            // 
            this.imagesOrTextListBox.FormattingEnabled = true;
            this.imagesOrTextListBox.Location = new System.Drawing.Point(6, 6);
            this.imagesOrTextListBox.Name = "imagesOrTextListBox";
            this.imagesOrTextListBox.Size = new System.Drawing.Size(120, 355);
            this.imagesOrTextListBox.TabIndex = 2;
            this.imagesOrTextListBox.SelectedIndexChanged += new System.EventHandler(this.ImagesOrTextListBox_SelectedIndexChanged);
            // 
            // MessageBox
            // 
            this.MessageBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MessageBox.Enabled = false;
            this.MessageBox.Location = new System.Drawing.Point(13, 428);
            this.MessageBox.Name = "MessageBox";
            this.MessageBox.Size = new System.Drawing.Size(915, 89);
            this.MessageBox.TabIndex = 3;
            this.MessageBox.Text = "";
            // 
            // updateTimer
            // 
            this.updateTimer.Enabled = true;
            this.updateTimer.Interval = 11;
            this.updateTimer.Tick += new System.EventHandler(this.UpdateTimer_Tick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 541);
            this.Controls.Add(this.MessageBox);
            this.Controls.Add(this.MainTabControl);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "Oculist Beta 2.0";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.MainTabControl.ResumeLayout(false);
            this.Configuration.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timerDelaySlider)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadius)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.TextEyeTracking.ResumeLayout(false);
            this.TabTextControler.ResumeLayout(false);
            this.tabTextDevelopmentMode.ResumeLayout(false);
            this.tabTextDevelopmentMode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textDevPointNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDevPosY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDevPosX)).EndInit();
            this.tabTextTrackingMode.ResumeLayout(false);
            this.tabTextTrackingMode.PerformLayout();
            this.boxTextExperimentControls.ResumeLayout(false);
            this.boxTextExperimentControls.PerformLayout();
            this.ImageEyeTracking.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.imageDevelopmentTextBox.ResumeLayout(false);
            this.imageDevelopmentTextBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageDevPointNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDevPosY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageDevPosX)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.boxImageExperimentControls.ResumeLayout(false);
            this.boxImageExperimentControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlPictureBox)).EndInit();
            this.EyeTrackerCorrection.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScreenEyeTrackingYOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScreenEyeTrackingXOffset)).EndInit();
            this.QuestionTab.ResumeLayout(false);
            this.QuestionTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQuestionSetup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inspectorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textPointDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imagePointDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage Configuration;
        private System.Windows.Forms.TabPage TextEyeTracking;
        private System.Windows.Forms.RichTextBox MessageBox;
        private System.Windows.Forms.TabPage ImageEyeTracking;
        private System.Windows.Forms.TabPage EyeTrackerCorrection;
        private System.Windows.Forms.TabPage QuestionTab;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox leftEyeXTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox leftEyeYTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox bothEyesXTextBox;
        private System.Windows.Forms.TextBox bothEyesYTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox rightEyeXTextBox;
        private System.Windows.Forms.TextBox rightEyeYTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox leftEyePupilDiameterTextBox;
        private System.Windows.Forms.TextBox rightEyePupilDiameterTextBox;
        private System.Windows.Forms.TextBox rightHeadEyeZTextBox;
        private System.Windows.Forms.TextBox rightHeadEyeYTextBox;
        private System.Windows.Forms.TextBox rightHeadEyeXTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox leftHeadEyeZTextBox;
        private System.Windows.Forms.TextBox leftHeadEyeYTextBox;
        private System.Windows.Forms.TextBox leftHeadEyeXTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox CheckboxAskQuestionsAfterSession;
        private System.Windows.Forms.CheckBox checkBoxDisplayReadjustedData;
        private System.Windows.Forms.CheckBox checkboxRecordRightEye;
        private System.Windows.Forms.CheckBox checkboxRecordLeftEyeData;
        private System.Windows.Forms.CheckBox checkBoxDevelopmentMode;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonRemoveText;
        private System.Windows.Forms.Button buttonAddText;
        private System.Windows.Forms.Button buttonRemoveImage;
        private System.Windows.Forms.Button buttonAddImage;
        private System.Windows.Forms.ComboBox comboBoxTextFiles;
        private System.Windows.Forms.ComboBox comboBoxImageFiles;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDownRadius;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabControl TabTextControler;
        private System.Windows.Forms.TabPage tabTextDevelopmentMode;
        private System.Windows.Forms.TabPage tabTextTrackingMode;
        private System.Windows.Forms.RichTextBox controlTextBox;
        private System.Windows.Forms.PictureBox controlPictureBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox timerDelayTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TrackBar timerDelaySlider;
        private System.Windows.Forms.Timer updateTimer;
        private System.Windows.Forms.CheckBox checkBoxLocalScreenPosition;
        private System.Windows.Forms.Label textYAxis;
        private System.Windows.Forms.Label textXAxis;
        private System.Windows.Forms.Label imageYAxis;
        private System.Windows.Forms.Label imageXAxis;
        private System.Windows.Forms.Label correctionYAxis;
        private System.Windows.Forms.Label correctionXAxis;
        private System.Windows.Forms.ListBox textDevelopmentListBox;
        private System.Windows.Forms.ListBox textExperimentListBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button buttonSetTextPoint;
        private System.Windows.Forms.NumericUpDown textDevPointNumber;
        private System.Windows.Forms.NumericUpDown textDevPosY;
        private System.Windows.Forms.NumericUpDown textDevPosX;
        private System.Windows.Forms.GroupBox boxTextExperimentControls;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button buttonRequestTextPermission;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button butonShowTextTrackingWindow;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage imageDevelopmentTextBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button buttonSetImagePoint;
        private System.Windows.Forms.NumericUpDown imageDevPointNumber;
        private System.Windows.Forms.NumericUpDown imageDevPosY;
        private System.Windows.Forms.NumericUpDown imageDevPosX;
        private System.Windows.Forms.ListBox imageDevelopmentListBox;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox boxImageExperimentControls;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button butonShowImageTrackingWindow;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button buttonRequestImagePermission;
        private System.Windows.Forms.ListBox imageExperimentListBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown numericUpDownScreenEyeTrackingYOffset;
        private System.Windows.Forms.NumericUpDown numericUpDownScreenEyeTrackingXOffset;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button pauseUnpauseTextSession;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button pauseUnpauseImageSession;
        private System.Windows.Forms.TextBox questionTextBox;
        private System.Windows.Forms.RadioButton radioButtonTextQuestions;
        private System.Windows.Forms.RadioButton radioButtonImagesQuestions;
        private System.Windows.Forms.ListBox imagesOrTextListBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.RichTextBox textBoxQuestions;
        private System.Windows.Forms.PictureBox pictureBoxQuestionSetup;
        private System.Windows.Forms.CheckBox checkBoxAskQuestionIfPointSeen;
        private System.Windows.Forms.ListBox listboxTextOrImagePoints;
        private System.Windows.Forms.Button buttonSetQuestion;
        private System.Windows.Forms.Label questionYAxis;
        private System.Windows.Forms.Label questionXAxis;
    }
}

