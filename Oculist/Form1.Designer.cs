﻿namespace Oculist
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inspectorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainDataWindow = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.DataWindow = new System.Windows.Forms.RichTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.btnAddWord = new System.Windows.Forms.Button();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnRemoveText = new System.Windows.Forms.Button();
            this.btnAddText = new System.Windows.Forms.Button();
            this.comboBoxText = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRemoveImage = new System.Windows.Forms.Button();
            this.btnAddImage = new System.Windows.Forms.Button();
            this.comboBoxImages = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.RadiusMargin = new System.Windows.Forms.NumericUpDown();
            this.checkQuestions = new System.Windows.Forms.CheckBox();
            this.checkRecordData = new System.Windows.Forms.CheckBox();
            this.checkLocalScreenPos = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.diameter_box = new System.Windows.Forms.TextBox();
            this.headZ_box = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.headY_box = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.name_box = new System.Windows.Forms.TextBox();
            this.headX_box = new System.Windows.Forms.TextBox();
            this.screenX_box = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.screenY_box = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ImageBoxCursorX = new System.Windows.Forms.Label();
            this.ImageBoxCursorY = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Reposition = new System.Windows.Forms.CheckBox();
            this.ImagesListBoxSorter = new System.Windows.Forms.ListBox();
            this.ImageNumericStrip = new System.Windows.Forms.NumericUpDown();
            this.btnSetPositionImage = new System.Windows.Forms.Button();
            this.ImageSelectPosY = new System.Windows.Forms.NumericUpDown();
            this.ImageSelectPosX = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnStartTrackingImages = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureDisplayBox = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.TextBoxCursorX = new System.Windows.Forms.Label();
            this.TextBoxCursorY = new System.Windows.Forms.Label();
            this.TrackerTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textReposition = new System.Windows.Forms.CheckBox();
            this.TextListBoxSorter = new System.Windows.Forms.ListBox();
            this.TextNumericStrip = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSetPositionText = new System.Windows.Forms.Button();
            this.TextSelectPosY = new System.Windows.Forms.NumericUpDown();
            this.TextSelectPosX = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnStartTrackingText = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.calibration = new System.Windows.Forms.TabPage();
            this.XCalibrationLine = new System.Windows.Forms.Label();
            this.YCalibrationLine = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.DelayDisplay = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.CalibrateEyeXBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.CalibrateEyeYBox = new System.Windows.Forms.TextBox();
            this.TimerFrequency = new System.Windows.Forms.TrackBar();
            this.RightEyeCalibrateSelected = new System.Windows.Forms.RadioButton();
            this.LeftEyeCalibrateSelected = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ErrorMessageStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.LeftEyeRadio = new System.Windows.Forms.RadioButton();
            this.RightEyeRadio = new System.Windows.Forms.RadioButton();
            this.BothEyesRadio = new System.Windows.Forms.RadioButton();
            this.Y_Offset = new System.Windows.Forms.NumericUpDown();
            this.X_Offset = new System.Windows.Forms.NumericUpDown();
            this.BothEyesCalibrate = new System.Windows.Forms.RadioButton();
            this.ShowWindowImage = new System.Windows.Forms.CheckBox();
            this.ShowWindowText = new System.Windows.Forms.CheckBox();
            this.questionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.MainDataWindow.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadiusMargin)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageNumericStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageSelectPosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageSelectPosX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureDisplayBox)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextNumericStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSelectPosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSelectPosX)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            this.calibration.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimerFrequency)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Y_Offset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X_Offset)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1012, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.NewToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.questionsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inspectorToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // inspectorToolStripMenuItem
            // 
            this.inspectorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imageDataToolStripMenuItem,
            this.textDataToolStripMenuItem});
            this.inspectorToolStripMenuItem.Name = "inspectorToolStripMenuItem";
            this.inspectorToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.inspectorToolStripMenuItem.Text = "Inspector";
            // 
            // imageDataToolStripMenuItem
            // 
            this.imageDataToolStripMenuItem.Name = "imageDataToolStripMenuItem";
            this.imageDataToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.imageDataToolStripMenuItem.Text = "ImageData";
            this.imageDataToolStripMenuItem.Click += new System.EventHandler(this.ImageDataToolStripMenuItem_Click);
            // 
            // textDataToolStripMenuItem
            // 
            this.textDataToolStripMenuItem.Name = "textDataToolStripMenuItem";
            this.textDataToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.textDataToolStripMenuItem.Text = "TextData";
            this.textDataToolStripMenuItem.Click += new System.EventHandler(this.TextDataToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // MainDataWindow
            // 
            this.MainDataWindow.Controls.Add(this.tabPage1);
            this.MainDataWindow.Controls.Add(this.tabPage2);
            this.MainDataWindow.Controls.Add(this.tabPage3);
            this.MainDataWindow.Controls.Add(this.tabPage4);
            this.MainDataWindow.Controls.Add(this.tabPage5);
            this.MainDataWindow.Controls.Add(this.calibration);
            this.MainDataWindow.Location = new System.Drawing.Point(0, 27);
            this.MainDataWindow.Name = "MainDataWindow";
            this.MainDataWindow.SelectedIndex = 0;
            this.MainDataWindow.ShowToolTips = true;
            this.MainDataWindow.Size = new System.Drawing.Size(1012, 509);
            this.MainDataWindow.TabIndex = 1;
            this.MainDataWindow.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.DataWindow);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1004, 483);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Setup";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // DataWindow
            // 
            this.DataWindow.Location = new System.Drawing.Point(9, 220);
            this.DataWindow.Name = "DataWindow";
            this.DataWindow.Size = new System.Drawing.Size(696, 257);
            this.DataWindow.TabIndex = 17;
            this.DataWindow.Text = "";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.btnAddWord);
            this.groupBox3.Controls.Add(this.comboBox3);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.btnRemoveText);
            this.groupBox3.Controls.Add(this.btnAddText);
            this.groupBox3.Controls.Add(this.comboBoxText);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.btnRemoveImage);
            this.groupBox3.Controls.Add(this.btnAddImage);
            this.groupBox3.Controls.Add(this.comboBoxImages);
            this.groupBox3.Location = new System.Drawing.Point(406, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(299, 206);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Files";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Word Documents";
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(214, 132);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 21;
            this.button5.Text = "Remove";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btnAddWord
            // 
            this.btnAddWord.Enabled = false;
            this.btnAddWord.Location = new System.Drawing.Point(133, 131);
            this.btnAddWord.Name = "btnAddWord";
            this.btnAddWord.Size = new System.Drawing.Size(75, 23);
            this.btnAddWord.TabIndex = 20;
            this.btnAddWord.Text = "Add";
            this.btnAddWord.UseVisualStyleBackColor = true;
            this.btnAddWord.Click += new System.EventHandler(this.Button6_Click);
            // 
            // comboBox3
            // 
            this.comboBox3.Enabled = false;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(6, 132);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 21);
            this.comboBox3.TabIndex = 19;
            /﻿namespace Oculist
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inspectorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainDataWindow = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.DataWindow = new System.Windows.Forms.RichTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.btnAddWord = new System.Windows.Forms.Button();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnRemoveText = new System.Windows.Forms.Button();
            this.btnAddText = new System.Windows.Forms.Button();
            this.comboBoxText = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnRemoveImage = new System.Windows.Forms.Button();
            this.btnAddImage = new System.Windows.Forms.Button();
            this.comboBoxImages = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.RadiusMargin = new System.Windows.Forms.NumericUpDown();
            this.checkQuestions = new System.Windows.Forms.CheckBox();
            this.checkRecordData = new System.Windows.Forms.CheckBox();
            this.checkLocalScreenPos = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.diameter_box = new System.Windows.Forms.TextBox();
            this.headZ_box = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.headY_box = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.name_box = new System.Windows.Forms.TextBox();
            this.headX_box = new System.Windows.Forms.TextBox();
            this.screenX_box = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.screenY_box = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ImageBoxCursorX = new System.Windows.Forms.Label();
            this.ImageBoxCursorY = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Reposition = new System.Windows.Forms.CheckBox();
            this.ImagesListBoxSorter = new System.Windows.Forms.ListBox();
            this.ImageNumericStrip = new System.Windows.Forms.NumericUpDown();
            this.btnSetPositionImage = new System.Windows.Forms.Button();
            this.ImageSelectPosY = new System.Windows.Forms.NumericUpDown();
            this.ImageSelectPosX = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnStartTrackingImages = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureDisplayBox = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.TextBoxCursorX = new System.Windows.Forms.Label();
            this.TextBoxCursorY = new System.Windows.Forms.Label();
            this.TrackerTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textReposition = new System.Windows.Forms.CheckBox();
            this.TextListBoxSorter = new System.Windows.Forms.ListBox();
            this.TextNumericStrip = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSetPositionText = new System.Windows.Forms.Button();
            this.TextSelectPosY = new System.Windows.Forms.NumericUpDown();
            this.TextSelectPosX = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnStartTrackingText = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.calibration = new System.Windows.Forms.TabPage();
            this.XCalibrationLine = new System.Windows.Forms.Label();
            this.YCalibrationLine = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.DelayDisplay = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.CalibrateEyeXBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.CalibrateEyeYBox = new System.Windows.Forms.TextBox();
            this.TimerFrequency = new System.Windows.Forms.TrackBar();
            this.RightEyeCalibrateSelected = new System.Windows.Forms.RadioButton();
            this.LeftEyeCalibrateSelected = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ErrorMessageStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.LeftEyeRadio = new System.Windows.Forms.RadioButton();
            this.RightEyeRadio = new System.Windows.Forms.RadioButton();
            this.BothEyesRadio = new System.Windows.Forms.RadioButton();
            this.Y_Offset = new System.Windows.Forms.NumericUpDown();
            this.X_Offset = new System.Windows.Forms.NumericUpDown();
            this.BothEyesCalibrate = new System.Windows.Forms.RadioButton();
            this.ShowWindowImage = new System.Windows.Forms.CheckBox();
            this.ShowWindowText = new System.Windows.Forms.CheckBox();
            this.questionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.MainDataWindow.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadiusMargin)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageNumericStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageSelectPosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageSelectPosX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureDisplayBox)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextNumericStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSelectPosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSelectPosX)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            this.calibration.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimerFrequency)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Y_Offset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X_Offset)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1012, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.NewToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.questionsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inspectorToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // inspectorToolStripMenuItem
            // 
            this.inspectorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imageDataToolStripMenuItem,
            this.textDataToolStripMenuItem});
            this.inspectorToolStripMenuItem.Name = "inspectorToolStripMenuItem";
            this.inspectorToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.inspectorToolStripMenuItem.Text = "Inspector";
            // 
            // imageDataToolStripMenuItem
            // 
            this.imageDataToolStripMenuItem.Name = "imageDataToolStripMenuItem";
            this.imageDataToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.imageDataToolStripMenuItem.Text = "ImageData";
            this.imageDataToolStripMenuItem.Click += new System.EventHandler(this.ImageDataToolStripMenuItem_Click);
            // 
            // textDataToolStripMenuItem
            // 
            this.textDataToolStripMenuItem.Name = "textDataToolStripMenuItem";
            this.textDataToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.textDataToolStripMenuItem.Text = "TextData";
            this.textDataToolStripMenuItem.Click += new System.EventHandler(this.TextDataToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // MainDataWindow
            // 
            this.MainDataWindow.Controls.Add(this.tabPage1);
            this.MainDataWindow.Controls.Add(this.tabPage2);
            this.MainDataWindow.Controls.Add(this.tabPage3);
            this.MainDataWindow.Controls.Add(this.tabPage4);
            this.MainDataWindow.Controls.Add(this.tabPage5);
            this.MainDataWindow.Controls.Add(this.calibration);
            this.MainDataWindow.Location = new System.Drawing.Point(0, 27);
            this.MainDataWindow.Name = "MainDataWindow";
            this.MainDataWindow.SelectedIndex = 0;
            this.MainDataWindow.ShowToolTips = true;
            this.MainDataWindow.Size = new System.Drawing.Size(1012, 509);
            this.MainDataWindow.TabIndex = 1;
            this.MainDataWindow.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.DataWindow);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1004, 483);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Setup";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // DataWindow
            // 
            this.DataWindow.Location = new System.Drawing.Point(9, 220);
            this.DataWindow.Name = "DataWindow";
            this.DataWindow.Size = new System.Drawing.Size(696, 257);
            this.DataWindow.TabIndex = 17;
            this.DataWindow.Text = "";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.btnAddWord);
            this.groupBox3.Controls.Add(this.comboBox3);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.btnRemoveText);
            this.groupBox3.Controls.Add(this.btnAddText);
            this.groupBox3.Controls.Add(this.comboBoxText);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.btnRemoveImage);
            this.groupBox3.Controls.Add(this.btnAddImage);
            this.groupBox3.Controls.Add(this.comboBoxImages);
            this.groupBox3.Location = new System.Drawing.Point(406, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(299, 206);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Files";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Word Documents";
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(214, 132);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 21;
            this.button5.Text = "Remove";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btnAddWord
            // 
            this.btnAddWord.Enabled = false;
            this.btnAddWord.Location = new System.Drawing.Point(133, 131);
            this.btnAddWord.Name = "btnAddWord";
            this.btnAddWord.Size = new System.Drawing.Size(75, 23);
            this.btnAddWord.TabIndex = 20;
            this.btnAddWord.Text = "Add";
            this.btnAddWord.UseVisualStyleBackColor = true;
            this.btnAddWord.Click += new System.EventHandler(this.Button6_Click);
            // 
            // comboBox3
            // 
            this.comboBox3.Enabled = false;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(6, 132);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 21);
            this.comboBox3.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Text files";
            // 
            // btnRemoveText
            // 
            this.btnRemoveText.Location = new System.Drawing.Point(214, 86);
            this.btnRemoveText.Name = "btnRemoveText";
            this.btnRemoveText.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveText.TabIndex = 17;
            this.btnRemoveText.Text = "Remove";
            this.btnRemoveText.UseVisualStyleBackColor = true;
            this.btnRemoveText.Click += new System.EventHandler(this.Button3_Click);
            // 
            // btnAddText
            // 
            this.btnAddText.Location = new System.Drawing.Point(133, 85);
            this.btnAddText.Name = "btnAddText";
            this.btnAddText.Size = new System.Drawing.Size(75, 23);
            this.btnAddText.TabIndex = 16;
            this.btnAddText.Text = "Add";
            this.btnAddText.UseVisualStyleBackColor = true;
            this.btnAddText.Click += new System.EventHandler(this.Button4_Click);
            // 
            // comboBoxText
            // 
            this.comboBoxText.FormattingEnabled = true;
            this.comboBoxText.Location = new System.Drawing.Point(6, 86);
            this.comboBoxText.Name = "comboBoxText";
            this.comboBoxText.Size = new System.Drawing.Size(121, 21);
            this.comboBoxText.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Images";
            // 
            // btnRemoveImage
            // 
            this.btnRemoveImage.Location = new System.Drawing.Point(214, 41);
            this.btnRemoveImage.Name = "btnRemoveImage";
            this.btnRemoveImage.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveImage.TabIndex = 2;
            this.btnRemoveImage.Text = "Remove";
            this.btnRemoveImage.UseVisualStyleBackColor = true;
            this.btnRemoveImage.Click += new System.EventHandler(this.Button2_Click);
            // 
            // btnAddImage
            // 
            this.btnAddImage.Location = new System.Drawing.Point(133, 40);
            this.btnAddImage.Name = "btnAddImage";
            this.btnAddImage.Size = new System.Drawing.Size(75, 23);
            this.btnAddImage.TabIndex = 1;
            this.btnAddImage.Text = "Add";
            this.btnAddImage.UseVisualStyleBackColor = true;
            this.btnAddImage.Click += new System.EventHandler(this.Button1_Click);
            // 
            // comboBoxImages
            // 
            this.comboBoxImages.FormattingEnabled = true;
            this.comboBoxImages.Location = new System.Drawing.Point(6, 41);
            this.comboBoxImages.Name = "comboBoxImages";
            this.comboBoxImages.Size = new System.Drawing.Size(121, 21);
            this.comboBoxImages.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BothEyesRadio);
            this.groupBox2.Controls.Add(this.RightEyeRadio);
            this.groupBox2.Controls.Add(this.LeftEyeRadio);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.RadiusMargin);
            this.groupBox2.Controls.Add(this.checkQuestions);
            this.groupBox2.Controls.Add(this.checkRecordData);
            this.groupBox2.Controls.Add(this.checkLocalScreenPos);
            this.groupBox2.Location = new System.Drawing.Point(199, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 206);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Setings";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(51, 161);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Radius";
            // 
            // RadiusMargin
            // 
            this.RadiusMargin.Location = new System.Drawing.Point(6, 159);
            this.RadiusMargin.Name = "RadiusMargin";
            this.RadiusMargin.Size = new System.Drawing.Size(39, 20);
            this.RadiusMargin.TabIndex = 6;
            // 
            // checkQuestions
            // 
            this.checkQuestions.AutoSize = true;
            this.checkQuestions.Location = new System.Drawing.Point(6, 135);
            this.checkQuestions.Name = "checkQuestions";
            this.checkQuestions.Size = new System.Drawing.Size(73, 17);
            this.checkQuestions.TabIndex = 5;
            this.checkQuestions.Text = "Questions";
            this.checkQuestions.UseVisualStyleBackColor = true;
            // 
            // checkRecordData
            // 
            this.checkRecordData.AutoSize = true;
            this.checkRecordData.Location = new System.Drawing.Point(6, 112);
            this.checkRecordData.Name = "checkRecordData";
            this.checkRecordData.Size = new System.Drawing.Size(84, 17);
            this.checkRecordData.TabIndex = 4;
            this.checkRecordData.Text = "RecordData";
            this.checkRecordData.UseVisualStyleBackColor = true;
            this.checkRecordData.CheckedChanged += new System.EventHandler(this.checkRecordData_CheckedChanged);
            // 
            // checkLocalScreenPos
            // 
            this.checkLocalScreenPos.AutoSize = true;
            this.checkLocalScreenPos.Location = new System.Drawing.Point(8, 86);
            this.checkLocalScreenPos.Name = "checkLocalScreenPos";
            this.checkLocalScreenPos.Size = new System.Drawing.Size(104, 17);
            this.checkLocalScreenPos.TabIndex = 2;
            this.checkLocalScreenPos.Text = "LocalScreenPos";
            this.checkLocalScreenPos.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.diameter_box);
            this.groupBox1.Controls.Add(this.headZ_box);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.headY_box);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.name_box);
            this.groupBox1.Controls.Add(this.headX_box);
            this.groupBox1.Controls.Add(this.screenX_box);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.screenY_box);
            this.groupBox1.Location = new System.Drawing.Point(9, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 206);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tracker Info";
            // 
            // diameter_box
            // 
            this.diameter_box.Location = new System.Drawing.Point(71, 45);
            this.diameter_box.Name = "diameter_box";
            this.diameter_box.Size = new System.Drawing.Size(100, 20);
            this.diameter_box.TabIndex = 1;
            // 
            // headZ_box
            // 
            this.headZ_box.Location = new System.Drawing.Point(71, 175);
            this.headZ_box.Name = "headZ_box";
            this.headZ_box.Size = new System.Drawing.Size(100, 20);
            this.headZ_box.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Diameter";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Head_Z";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name";
            // 
            // headY_box
            // 
            this.headY_box.Location = new System.Drawing.Point(71, 149);
            this.headY_box.Name = "headY_box";
            this.headY_box.Size = new System.Drawing.Size(100, 20);
            this.headY_box.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Head_Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Screen_X";
            // 
            // name_box
            // 
            this.name_box.Location = new System.Drawing.Point(71, 19);
            this.name_box.Name = "name_box";
            this.name_box.Size = new System.Drawing.Size(100, 20);
            this.name_box.TabIndex = 3;
            // 
            // headX_box
            // 
            this.headX_box.Location = new System.Drawing.Point(71, 123);
            this.headX_box.Name = "headX_box";
            this.headX_box.Size = new System.Drawing.Size(100, 20);
            this.headX_box.TabIndex = 9;
            // 
            // screenX_box
            // 
            this.screenX_box.Location = new System.Drawing.Point(71, 71);
            this.screenX_box.Name = "screenX_box";
            this.screenX_box.Size = new System.Drawing.Size(100, 20);
            this.screenX_box.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Head_X";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Screen_Y";
            // 
            // screenY_box
            // 
            this.screenY_box.Location = new System.Drawing.Point(71, 97);
            this.screenY_box.Name = "screenY_box";
            this.screenY_box.Size = new System.Drawing.Size(100, 20);
            this.screenY_box.TabIndex = 7;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.Controls.Add(this.ImageBoxCursorX);
            this.tabPage2.Controls.Add(this.ImageBoxCursorY);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.pictureDisplayBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1004, 483);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Image Eye tracking";
            // 
            // ImageBoxCursorX
            // 
            this.ImageBoxCursorX.BackColor = System.Drawing.Color.Black;
            this.ImageBoxCursorX.Location = new System.Drawing.Point(351, 17);
            this.ImageBoxCursorX.Name = "ImageBoxCursorX";
            this.ImageBoxCursorX.Size = new System.Drawing.Size(2, 345);
            this.ImageBoxCursorX.TabIndex = 3;
            this.ImageBoxCursorX.Text = "label20";
            // 
            // ImageBoxCursorY
            // 
            this.ImageBoxCursorY.BackColor = System.Drawing.Color.Black;
            this.ImageBoxCursorY.Location = new System.Drawing.Point(314, 346);
            this.ImageBoxCursorY.Name = "ImageBoxCursorY";
            this.ImageBoxCursorY.Size = new System.Drawing.Size(512, 2);
            this.ImageBoxCursorY.TabIndex = 2;
            this.ImageBoxCursorY.Text = "X";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ShowWindowImage);
            this.groupBox4.Controls.Add(this.Reposition);
            this.groupBox4.Controls.Add(this.ImagesListBoxSorter);
            this.groupBox4.Controls.Add(this.ImageNumericStrip);
            this.groupBox4.Controls.Add(this.btnSetPositionImage);
            this.groupBox4.Controls.Add(this.ImageSelectPosY);
            this.groupBox4.Controls.Add(this.ImageSelectPosX);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.btnStartTrackingImages);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Location = new System.Drawing.Point(832, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(166, 471);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Input/Output";
            // 
            // Reposition
            // 
            this.Reposition.AutoSize = true;
            this.Reposition.Location = new System.Drawing.Point(96, 23);
            this.Reposition.Name = "Reposition";
            this.Reposition.Size = new System.Drawing.Size(76, 17);
            this.Reposition.TabIndex = 16;
            this.Reposition.Text = "Reposition";
            this.Reposition.UseVisualStyleBackColor = true;
            this.Reposition.CheckedChanged += new System.EventHandler(this.Reposition_CheckedChanged);
            // 
            // ImagesListBoxSorter
            // 
            this.ImagesListBoxSorter.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ImagesListBoxSorter.FormattingEnabled = true;
            this.ImagesListBoxSorter.Location = new System.Drawing.Point(7, 152);
            this.ImagesListBoxSorter.Name = "ImagesListBoxSorter";
            this.ImagesListBoxSorter.Size = new System.Drawing.Size(152, 225);
            this.ImagesListBoxSorter.TabIndex = 15;
            this.ImagesListBoxSorter.SelectedIndexChanged += new System.EventHandler(this.ImagesListBoxSorter_SelectedIndexChanged);
            // 
            // ImageNumericStrip
            // 
            this.ImageNumericStrip.Location = new System.Drawing.Point(49, 20);
            this.ImageNumericStrip.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.ImageNumericStrip.Name = "ImageNumericStrip";
            this.ImageNumericStrip.Size = new System.Drawing.Size(40, 20);
            this.ImageNumericStrip.TabIndex = 14;
            // 
            // btnSetPositionImage
            // 
            this.btnSetPositionImage.Location = new System.Drawing.Point(6, 86);
            this.btnSetPositionImage.Name = "btnSetPositionImage";
            this.btnSetPositionImage.Size = new System.Drawing.Size(153, 59);
            this.btnSetPositionImage.TabIndex = 13;
            this.btnSetPositionImage.Text = "Set";
            this.btnSetPositionImage.UseVisualStyleBackColor = true;
            this.btnSetPositionImage.Click += new System.EventHandler(this.BtnSetPositionImage_Click);
            // 
            // ImageSelectPosY
            // 
            this.ImageSelectPosY.Location = new System.Drawing.Point(94, 60);
            this.ImageSelectPosY.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.ImageSelectPosY.Name = "ImageSelectPosY";
            this.ImageSelectPosY.Size = new System.Drawing.Size(51, 20);
            this.ImageSelectPosY.TabIndex = 12;
            // 
            // ImageSelectPosX
            // 
            this.ImageSelectPosX.Location = new System.Drawing.Point(9, 60);
            this.ImageSelectPosX.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.ImageSelectPosX.Name = "ImageSelectPosX";
            this.ImageSelectPosX.Size = new System.Drawing.Size(51, 20);
            this.ImageSelectPosX.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(91, 43);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Position Y";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 43);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "Position X";
            // 
            // btnStartTrackingImages
            // 
            this.btnStartTrackingImages.Location = new System.Drawing.Point(7, 406);
            this.btnStartTrackingImages.Name = "btnStartTrackingImages";
            this.btnStartTrackingImages.Size = new System.Drawing.Size(153, 59);
            this.btnStartTrackingImages.TabIndex = 2;
            this.btnStartTrackingImages.Text = "Start";
            this.btnStartTrackingImages.UseVisualStyleBackColor = true;
            this.btnStartTrackingImages.Click += new System.EventHandler(this.btnStartTrackingImages_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "PointNr";
            // 
            // pictureDisplayBox
            // 
            this.pictureDisplayBox.BackColor = System.Drawing.Color.LightGray;
            this.pictureDisplayBox.Location = new System.Drawing.Point(314, 17);
            this.pictureDisplayBox.Name = "pictureDisplayBox";
            this.pictureDisplayBox.Size = new System.Drawing.Size(512, 345);
            this.pictureDisplayBox.TabIndex = 0;
            this.pictureDisplayBox.TabStop = false;
            this.pictureDisplayBox.Click += new System.EventHandler(this.PictureDisplayBox_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.TextBoxCursorX);
            this.tabPage3.Controls.Add(this.TextBoxCursorY);
            this.tabPage3.Controls.Add(this.TrackerTextBox);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1004, 483);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Text Eye Tracking";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // TextBoxCursorX
            // 
            this.TextBoxCursorX.BackColor = System.Drawing.Color.Black;
            this.TextBoxCursorX.Location = new System.Drawing.Point(84, 6);
            this.TextBoxCursorX.Name = "TextBoxCursorX";
            this.TextBoxCursorX.Size = new System.Drawing.Size(2, 458);
            this.TextBoxCursorX.TabIndex = 5;
            this.TextBoxCursorX.Text = "label20";
            // 
            // TextBoxCursorY
            // 
            this.TextBoxCursorY.BackColor = System.Drawing.Color.Black;
            this.TextBoxCursorY.Location = new System.Drawing.Point(79, 398);
            this.TextBoxCursorY.Name = "TextBoxCursorY";
            this.TextBoxCursorY.Size = new System.Drawing.Size(716, 2);
            this.TextBoxCursorY.TabIndex = 4;
            this.TextBoxCursorY.Text = "X";
            // 
            // TrackerTextBox
            // 
            this.TrackerTextBox.Location = new System.Drawing.Point(77, 6);
            this.TrackerTextBox.Name = "TrackerTextBox";
            this.TrackerTextBox.Size = new System.Drawing.Size(720, 460);
            this.TrackerTextBox.TabIndex = 3;
            this.TrackerTextBox.Text = "";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ShowWindowText);
            this.groupBox5.Controls.Add(this.textReposition);
            this.groupBox5.Controls.Add(this.TextListBoxSorter);
            this.groupBox5.Controls.Add(this.TextNumericStrip);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.btnSetPositionText);
            this.groupBox5.Controls.Add(this.TextSelectPosY);
            this.groupBox5.Controls.Add(this.TextSelectPosX);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.btnStartTrackingText);
            this.groupBox5.Location = new System.Drawing.Point(833, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(166, 471);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Input/Output";
            // 
            // textReposition
            // 
            this.textReposition.AutoSize = true;
            this.textReposition.Location = new System.Drawing.Point(93, 12);
            this.textReposition.Name = "textReposition";
            this.textReposition.Size = new System.Drawing.Size(76, 17);
            this.textReposition.TabIndex = 18;
            this.textReposition.Text = "Reposition";
            this.textReposition.UseVisualStyleBackColor = true;
            // 
            // TextListBoxSorter
            // 
            this.TextListBoxSorter.FormattingEnabled = true;
            this.TextListBoxSorter.Location = new System.Drawing.Point(6, 139);
            this.TextListBoxSorter.Name = "TextListBoxSorter";
            this.TextListBoxSorter.Size = new System.Drawing.Size(162, 238);
            this.TextListBoxSorter.TabIndex = 17;
            this.TextListBoxSorter.SelectedIndexChanged += new System.EventHandler(this.TextListBoxSorter_SelectedIndexChanged);
            // 
            // TextNumericStrip
            // 
            this.TextNumericStrip.Location = new System.Drawing.Point(47, 14);
            this.TextNumericStrip.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.TextNumericStrip.Name = "TextNumericStrip";
            this.TextNumericStrip.Size = new System.Drawing.Size(40, 20);
            this.TextNumericStrip.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "PointNr";
            // 
            // btnSetPositionText
            // 
            this.btnSetPositionText.Location = new System.Drawing.Point(6, 80);
            this.btnSetPositionText.Name = "btnSetPositionText";
            this.btnSetPositionText.Size = new System.Drawing.Size(153, 59);
            this.btnSetPositionText.TabIndex = 9;
            this.btnSetPositionText.Text = "Set";
            this.btnSetPositionText.UseVisualStyleBackColor = true;
            this.btnSetPositionText.Click += new System.EventHandler(this.BtnSetPositionText_Click);
            // 
            // TextSelectPosY
            // 
            this.TextSelectPosY.Location = new System.Drawing.Point(94, 54);
            this.TextSelectPosY.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.TextSelectPosY.Name = "TextSelectPosY";
            this.TextSelectPosY.Size = new System.Drawing.Size(51, 20);
            this.TextSelectPosY.TabIndex = 8;
            // 
            // TextSelectPosX
            // 
            this.TextSelectPosX.Location = new System.Drawing.Point(9, 54);
            this.TextSelectPosX.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.TextSelectPosX.Name = "TextSelectPosX";
            this.TextSelectPosX.Size = new System.Drawing.Size(51, 20);
            this.TextSelectPosX.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(91, 37);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Position Y";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Position X";
            // 
            // btnStartTrackingText
            // 
            this.btnStartTrackingText.Location = new System.Drawing.Point(7, 406);
            this.btnStartTrackingText.Name = "btnStartTrackingText";
            this.btnStartTrackingText.Size = new System.Drawing.Size(153, 59);
            this.btnStartTrackingText.TabIndex = 2;
            this.btnStartTrackingText.Text = "Start";
            this.btnStartTrackingText.UseVisualStyleBackColor = true;
            this.btnStartTrackingText.Click += new System.EventHandler(this.BtnStartTrackingText_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1004, 483);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Word Eye Tracking";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button9);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.numericUpDown3);
            this.groupBox6.Location = new System.Drawing.Point(832, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(166, 471);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Input/Output";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(7, 406);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(153, 59);
            this.button9.TabIndex = 2;
            this.button9.Text = "Start";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Delay(Seconds)";
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(95, 20);
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(65, 20);
            this.numericUpDown3.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1004, 483);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Eksperimenti";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // calibration
            // 
            this.calibration.Controls.Add(this.XCalibrationLine);
            this.calibration.Controls.Add(this.YCalibrationLine);
            this.calibration.Controls.Add(this.panel1);
            this.calibration.Controls.Add(this.groupBox7);
            this.calibration.Location = new System.Drawing.Point(4, 22);
            this.calibration.Name = "calibration";
            this.calibration.Padding = new System.Windows.Forms.Padding(3);
            this.calibration.Size = new System.Drawing.Size(1004, 483);
            this.calibration.TabIndex = 5;
            this.calibration.Text = "Calibrator";
            this.calibration.UseVisualStyleBackColor = true;
            // 
            // XCalibrationLine
            // 
            this.XCalibrationLine.BackColor = System.Drawing.Color.Black;
            this.XCalibrationLine.Location = new System.Drawing.Point(3, -6);
            this.XCalibrationLine.Name = "XCalibrationLine";
            this.XCalibrationLine.Size = new System.Drawing.Size(3, 518);
            this.XCalibrationLine.TabIndex = 6;
            this.XCalibrationLine.Text = "XCalibrationLine";
            // 
            // YCalibrationLine
            // 
            this.YCalibrationLine.BackColor = System.Drawing.Color.Black;
            this.YCalibrationLine.Location = new System.Drawing.Point(-13, 473);
            this.YCalibrationLine.Name = "YCalibrationLine";
            this.YCalibrationLine.Size = new System.Drawing.Size(844, 3);
            this.YCalibrationLine.TabIndex = 5;
            this.YCalibrationLine.Text = "X";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(384, 214);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(40, 40);
            this.panel1.TabIndex = 4;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.BothEyesCalibrate);
            this.groupBox7.Controls.Add(this.label25);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Controls.Add(this.DelayDisplay);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.CalibrateEyeXBox);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.CalibrateEyeYBox);
            this.groupBox7.Controls.Add(this.TimerFrequency);
            this.groupBox7.Controls.Add(this.RightEyeCalibrateSelected);
            this.groupBox7.Controls.Add(this.LeftEyeCalibrateSelected);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.Y_Offset);
            this.groupBox7.Controls.Add(this.X_Offset);
            this.groupBox7.Location = new System.Drawing.Point(830, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(166, 471);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Offset Regulator";
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(6, 283);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(154, 44);
            this.label25.TabIndex = 14;
            this.label25.Text = "NOTE! Make sure you have selected LocalScreenPos in Setup menu!";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 100);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 13);
            this.label24.TabIndex = 13;
            this.label24.Text = "Delay";
            // 
            // DelayDisplay
            // 
            this.DelayDisplay.Location = new System.Drawing.Point(7, 116);
            this.DelayDisplay.Name = "DelayDisplay";
            this.DelayDisplay.Size = new System.Drawing.Size(33, 20);
            this.DelayDisplay.TabIndex = 12;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1, 195);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 13);
            this.label22.TabIndex = 8;
            this.label22.Text = "Screen_X";
            // 
            // CalibrateEyeXBox
            // 
            this.CalibrateEyeXBox.Location = new System.Drawing.Point(60, 192);
            this.CalibrateEyeXBox.Name = "CalibrateEyeXBox";
            this.CalibrateEyeXBox.Size = new System.Drawing.Size(100, 20);
            this.CalibrateEyeXBox.TabIndex = 9;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(1, 221);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 13);
            this.label23.TabIndex = 10;
            this.label23.Text = "Screen_Y";
            // 
            // CalibrateEyeYBox
            // 
            this.CalibrateEyeYBox.Location = new System.Drawing.Point(60, 218);
            this.CalibrateEyeYBox.Name = "CalibrateEyeYBox";
            this.CalibrateEyeYBox.Size = new System.Drawing.Size(100, 20);
            this.CalibrateEyeYBox.TabIndex = 11;
            // 
            // TimerFrequency
            // 
            this.TimerFrequency.Location = new System.Drawing.Point(7, 140);
            this.TimerFrequency.Maximum = 120;
            this.TimerFrequency.Minimum = 1;
            this.TimerFrequency.Name = "TimerFrequency";
            this.TimerFrequency.Size = new System.Drawing.Size(153, 45);
            this.TimerFrequency.TabIndex = 6;
            this.TimerFrequency.Value = 1;
            this.TimerFrequency.Scroll += new System.EventHandler(this.TimerFrequency_Scroll);
            // 
            // RightEyeCalibrateSelected
            // 
            this.RightEyeCalibrateSelected.AutoSize = true;
            this.RightEyeCalibrateSelected.Location = new System.Drawing.Point(86, 94);
            this.RightEyeCalibrateSelected.Name = "RightEyeCalibrateSelected";
            this.RightEyeCalibrateSelected.Size = new System.Drawing.Size(68, 17);
            this.RightEyeCalibrateSelected.TabIndex = 5;
            this.RightEyeCalibrateSelected.Text = "RightEye";
            this.RightEyeCalibrateSelected.UseVisualStyleBackColor = true;
            this.RightEyeCalibrateSelected.CheckedChanged += new System.EventHandler(this.RightEyeSelected_CheckedChanged);
            // 
            // LeftEyeCalibrateSelected
            // 
            this.LeftEyeCalibrateSelected.AutoSize = true;
            this.LeftEyeCalibrateSelected.Checked = true;
            this.LeftEyeCalibrateSelected.Location = new System.Drawing.Point(86, 71);
            this.LeftEyeCalibrateSelected.Name = "LeftEyeCalibrateSelected";
            this.LeftEyeCalibrateSelected.Size = new System.Drawing.Size(61, 17);
            this.LeftEyeCalibrateSelected.TabIndex = 4;
            this.LeftEyeCalibrateSelected.TabStop = true;
            this.LeftEyeCalibrateSelected.Text = "LeftEye";
            this.LeftEyeCalibrateSelected.UseVisualStyleBackColor = true;
            this.LeftEyeCalibrateSelected.CheckedChanged += new System.EventHandler(this.LeftEyeSelected_CheckedChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(35, 47);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Y-Offset";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(35, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "X-Offset";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ErrorMessageStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 539);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1012, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ErrorMessageStatusLabel
            // 
            this.ErrorMessageStatusLabel.Name = "ErrorMessageStatusLabel";
            this.ErrorMessageStatusLabel.Size = new System.Drawing.Size(91, 17);
            this.ErrorMessageStatusLabel.Text = "Default Optimal";
            // 
            // updateTimer
            // 
            this.updateTimer.Enabled = true;
            this.updateTimer.Interval = 30;
            this.updateTimer.Tick += new System.EventHandler(this.UpdateTimer_Tick);
            // 
            // LeftEyeRadio
            // 
            this.LeftEyeRadio.AutoSize = true;
            this.LeftEyeRadio.Checked = true;
            this.LeftEyeRadio.Location = new System.Drawing.Point(6, 17);
            this.LeftEyeRadio.Name = "LeftEyeRadio";
            this.LeftEyeRadio.Size = new System.Drawing.Size(82, 17);
            this.LeftEyeRadio.TabIndex = 15;
            this.LeftEyeRadio.TabStop = true;
            this.LeftEyeRadio.Text = "LeftEyeOnly";
            this.LeftEyeRadio.UseVisualStyleBackColor = true;
            // 
            // RightEyeRadio
            // 
            this.RightEyeRadio.AutoSize = true;
            this.RightEyeRadio.Location = new System.Drawing.Point(6, 40);
            this.RightEyeRadio.Name = "RightEyeRadio";
            this.RightEyeRadio.Size = new System.Drawing.Size(89, 17);
            this.RightEyeRadio.TabIndex = 16;
            this.RightEyeRadio.Text = "RightEyeOnly";
            this.RightEyeRadio.UseVisualStyleBackColor = true;
            // 
            // BothEyesRadio
            // 
            this.BothEyesRadio.AutoSize = true;
            this.BothEyesRadio.Location = new System.Drawing.Point(6, 62);
            this.BothEyesRadio.Name = "BothEyesRadio";
            this.BothEyesRadio.Size = new System.Drawing.Size(117, 17);
            this.BothEyesRadio.TabIndex = 17;
            this.BothEyesRadio.Text = "BothEyesCombined";
            this.BothEyesRadio.UseVisualStyleBackColor = true;
            // 
            // Y_Offset
            // 
            this.Y_Offset.Location = new System.Drawing.Point(86, 45);
            this.Y_Offset.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.Y_Offset.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.Y_Offset.Name = "Y_Offset";
            this.Y_Offset.Size = new System.Drawing.Size(74, 20);
            this.Y_Offset.TabIndex = 1;
            // 
            // X_Offset
            // 
            this.X_Offset.Location = new System.Drawing.Point(86, 19);
            this.X_Offset.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.X_Offset.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.X_Offset.Name = "X_Offset";
            this.X_Offset.Size = new System.Drawing.Size(74, 20);
            this.X_Offset.TabIndex = 0;
            // 
            // BothEyesCalibrate
            // 
            this.BothEyesCalibrate.AutoSize = true;
            this.BothEyesCalibrate.Location = new System.Drawing.Point(86, 117);
            this.BothEyesCalibrate.Name = "BothEyesCalibrate";
            this.BothEyesCalibrate.Size = new System.Drawing.Size(70, 17);
            this.BothEyesCalibrate.TabIndex = 18;
            this.BothEyesCalibrate.Text = "BothEyes";
            this.BothEyesCalibrate.UseVisualStyleBackColor = true;
            this.BothEyesCalibrate.CheckedChanged += new System.EventHandler(this.BothEyesCalibrate_CheckedChanged);
            // 
            // ShowWindowImage
            // 
            this.ShowWindowImage.AutoSize = true;
            this.ShowWindowImage.Location = new System.Drawing.Point(9, 383);
            this.ShowWindowImage.Name = "ShowWindowImage";
            this.ShowWindowImage.Size = new System.Drawing.Size(92, 17);
            this.ShowWindowImage.TabIndex = 17;
            this.ShowWindowImage.Text = "ShowWindow";
            this.ShowWindowImage.UseVisualStyleBackColor = true;
            // 
            // ShowWindowText
            // 
            this.ShowWindowText.AutoSize = true;
            this.ShowWindowText.Location = new System.Drawing.Point(9, 383);
            this.ShowWindowText.Name = "ShowWindowText";
            this.ShowWindowText.Size = new System.Drawing.Size(92, 17);
            this.ShowWindowText.TabIndex = 19;
            this.ShowWindowText.Text = "ShowWindow";
            this.ShowWindowText.UseVisualStyleBackColor = true;
            // 
            // questionsToolStripMenuItem
            // 
            this.questionsToolStripMenuItem.Name = "questionsToolStripMenuItem";
            this.questionsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.questionsToolStripMenuItem.Text = "Questions";
            this.questionsToolStripMenuItem.Click += new System.EventHandler(this.questionsToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 561);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.MainDataWindow);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Oculist Beta 1.2";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.MainDataWindow.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadiusMargin)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageNumericStrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageSelectPosY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageSelectPosX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureDisplayBox)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextNumericStrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSelectPosY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSelectPosX)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            this.calibration.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimerFrequency)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Y_Offset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X_Offset)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.TabControl MainDataWindow;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TextBox screenY_box;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox screenX_box;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox name_box;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox diameter_box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkQuestions;
        private System.Windows.Forms.CheckBox checkRecordData;
        private System.Windows.Forms.CheckBox checkLocalScreenPos;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox headZ_box;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox headY_box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox headX_box;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureDisplayBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnAddWord;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnRemoveText;
        private System.Windows.Forms.Button btnAddText;
        private System.Windows.Forms.ComboBox comboBoxText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnRemoveImage;
        private System.Windows.Forms.Button btnAddImage;
        private System.Windows.Forms.ComboBox comboBoxImages;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnStartTrackingImages;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnStartTrackingText;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Timer updateTimer;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.RichTextBox DataWindow;
        private System.Windows.Forms.ToolStripMenuItem inspectorToolStripMenuItem;
        private System.Windows.Forms.RichTextBox TrackerTextBox;
        private System.Windows.Forms.NumericUpDown ImageSelectPosY;
        private System.Windows.Forms.NumericUpDown ImageSelectPosX;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown TextSelectPosY;
        private System.Windows.Forms.NumericUpDown TextSelectPosX;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnSetPositionImage;
        private System.Windows.Forms.Button btnSetPositionText;
        private System.Windows.Forms.NumericUpDown ImageNumericStrip;
        private System.Windows.Forms.NumericUpDown TextNumericStrip;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ToolStripStatusLabel ErrorMessageStatusLabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown RadiusMargin;
        private System.Windows.Forms.ListBox ImagesListBoxSorter;
        private System.Windows.Forms.ListBox TextListBoxSorter;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.CheckBox Reposition;
        private System.Windows.Forms.ToolStripMenuItem imageDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textDataToolStripMenuItem;
        private System.Windows.Forms.CheckBox textReposition;
        private System.Windows.Forms.Label TextBoxCursorX;
        private System.Windows.Forms.Label TextBoxCursorY;
        private System.Windows.Forms.TabPage calibration;
        private System.Windows.Forms.Label ImageBoxCursorX;
        private System.Windows.Forms.Label ImageBoxCursorY;
        private System.Windows.Forms.Label XCalibrationLine;
        private System.Windows.Forms.Label YCalibrationLine;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RadioButton RightEyeCalibrateSelected;
        private System.Windows.Forms.RadioButton LeftEyeCalibrateSelected;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox CalibrateEyeXBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox CalibrateEyeYBox;
        private System.Windows.Forms.TrackBar TimerFrequency;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox DelayDisplay;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.RadioButton BothEyesRadio;
        private System.Windows.Forms.RadioButton RightEyeRadio;
        private System.Windows.Forms.RadioButton LeftEyeRadio;
        private System.Windows.Forms.RadioButton BothEyesCalibrate;
        private System.Windows.Forms.NumericUpDown Y_Offset;
        private System.Windows.Forms.NumericUpDown X_Offset;
        private System.Windows.Forms.CheckBox ShowWindowImage;
        private System.Windows.Forms.CheckBox ShowWindowText;
        private System.Windows.Forms.ToolStripMenuItem questionsToolStripMenuItem;
    }
}



