﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tobii.Research;


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tobii.Research;


namespace Oculist
{
    public class Oculist
    {
        EyeTrackerCollection eyeTrackers = EyeTrackingOperations.FindAllEyeTrackers();
        IEyeTracker tracker;
        DisplayArea displayArea;


        private int leftScreenX, leftScreenY, rightScreenX, rightScreenY;
        private float rawleftScreenX, rawleftScreenY, rawrightScreenX, rawrightScreenY;
        private int leftHeadX, leftHeadY, leftHeadZ, rightHeadX, rightHeadY, rightHeadZ;
        private float leftEyeDiameter, rightEyeDiameter;

        int w = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
        int h = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

        // Constructor
        public Oculist()
        {
            tracker = eyeTrackers.First<IEyeTracker>();

            displayArea = tracker.GetDisplayArea();
            EyeTrackerScreenTracking();
            EyeTrackerHeadTracking();

        }

        public void Update()
        {
            EyeTrackerDataReformating();
        }

        public void EyeTrackerScreenTracking()
        {

            tracker.GazeDataReceived += (s, e) =>
            {
                rawleftScreenX = e.LeftEye.GazePoint.PositionOnDisplayArea.X;
                rawleftScreenY = e.LeftEye.GazePoint.PositionOnDisplayArea.Y;
                rawrightScreenX = e.RightEye.GazePoint.PositionOnDisplayArea.X;
                rawrightScreenY = e.RightEye.GazePoint.PositionOnDisplayArea.Y;
                leftEyeDiameter = e.LeftEye.Pupil.PupilDiameter;
                rightEyeDiameter = e.RightEye.Pupil.PupilDiameter;

            };
        }

        public void EyeTrackerHeadTracking()
        {
            tracker.GazeDataReceived += (s, e) =>
            {
                leftHeadX = (int)e.LeftEye.GazeOrigin.PositionInUserCoordinates.X;
                leftHeadY = (int)e.LeftEye.GazeOrigin.PositionInUserCoordinates.Y;
                leftHeadZ = (int)e.LeftEye.GazeOrigin.PositionInUserCoordinates.Z;

                rightHeadX = (int)e.RightEye.GazeOrigin.PositionInUserCoordinates.X;
                rightHeadY = (int)e.RightEye.GazeOrigin.PositionInUserCoordinates.Y;
                rightHeadZ = (int)e.RightEye.GazeOrigin.PositionInUserCoordinates.Z;

            };
        }

        public void EyeTrackerDataReformating()
        {
            leftScreenX = (int)(rawleftScreenX * w);
            leftScreenY = (int)(rawleftScreenY * h);
            rightScreenX = (int)(rawrightScreenX * w);
            rightScreenY = (int)(rawrightScreenY * h);

            if (leftScreenX < 0) leftScreenX = 1;
            if (rightScreenX < 0) rightScreenX = 1;
            if (leftScreenY < 0) leftScreenY = 1;
            if (rightScreenY < 0) rightScreenY = 1;
        }



        public string DisplayAreaData()
        {
            return "Eye Tracker Display Area:\nTopLeft = " + displayArea.TopLeft.X + " " + displayArea.TopLeft.Y + " " + displayArea.TopLeft.Z + "\nBottomLeft = " + displayArea.BottomLeft.X + " " + displayArea.BottomLeft.Y + " " + displayArea.BottomLeft.Z + "\nTopRight = " + displayArea.TopRight.X + " " + displayArea.TopRight.Y + " " + displayArea.TopRight.Z + "\nBottomRight = " + displayArea.BottomRight.X + " " + displayArea.BottomRight.Y + " " + displayArea.BottomRight.Z+ "\n";
        }

        public string ActualDisplayScreenData()
        {
            return "Monitor screen resolution data:\nWidth =" + w + "\nHeight = " + h + "\n";
        }

        public string EyeTrackerData()
        {
            string pt0 = "Address: " + tracker.Address + "\nModel: " + tracker.Model + "\nSerialNumber: " + tracker.SerialNumber + "\nFirmwareVersion: " + tracker.FirmwareVersion + "\nRuntimeVersion: " + tracker.RuntimeVersion + "\nDeviceName: " + tracker.DeviceName;
            string pt1 = "\nGaze Output Frequency: " + tracker.GetGazeOutputFrequency().ToString();
            return pt0 + pt1;
        }

        #region Get data from Eye Tracker

        
        public int GetScreenLeftX()
        {
            return leftScreenX;
        }

        public int GetScreenLeftY()
        {
            return leftScreenY;
        }
        public int GetScreenRightX()
        {
            return rightScreenX;
        }
        public int GetScreenRightY()
        {
            return rightScreenY;
        }

        public int GetHeadLeftX()
        {
            return leftHeadX;
        }

        public int GetHeadLeftY()
        {
            return leftHeadY;
        }
        public int GetHeadLeftZ()
        {
            return leftHeadZ;
        }
        public int GetHeadRightX()
        {
            return rightHeadX;
        }
        public int GetHeadRightY()
        {
            return rightHeadY;
        }
        public int GetHeadRightZ()
        {
            return rightHeadZ;
        }
        public float GetLeftEyeDiameter()
        {
            return leftEyeDiameter;
        }
        public float GetRightEyeDiameter()
        {
            return rightEyeDiameter;
        }

        #endregion
    }
}
