﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using Oculist.Structure;
using MathNet.Filtering.Kalman;


namespace Oculist
{
 

    public partial class Form1 : Form
    {
        Oculist oculist;
        ContentManager contentManager = new ContentManager();
        TrackingComparator trackerComparator = new TrackingComparator();
        ImageDisplayer imageDisplayer = new ImageDisplayer();
        TextDisplayer textDisplayer = new TextDisplayer();
        EventLogger events = new EventLogger();
        public static string PROJECTNAME;


        public PointDataTXT textFiles = new PointDataTXT();
        public PointDataIMG imageFiles = new PointDataIMG();

        private bool startImageTracking = false;
        private bool startTextyTracking = false;

        private int ba﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using Oculist.Structure;
using MathNet.Filtering.Kalman;


namespace Oculist
{
 

    public partial class Form1 : Form
    {
        Oculist oculist;
        ContentManager contentManager = new ContentManager();
        TrackingComparator trackerComparator = new TrackingComparator();
        ImageDisplayer imageDisplayer = new ImageDisplayer();
        TextDisplayer textDisplayer = new TextDisplayer();
        EventLogger events = new EventLogger();
        public static string PROJECTNAME;


        public PointDataTXT textFiles = new PointDataTXT();
        public PointDataIMG imageFiles = new PointDataIMG();

        private bool startImageTracking = false;
        private bool startTextyTracking = false;

        private int baseDelayInterval;
        private int screenX, screenY;


        public Form1()
        {
            
            
        }

        public Form1(Oculist oculist)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            //this.MinimizeBox = false;
            InitializeComponent();
            this.oculist = oculist;

            DataWindow.Text += oculist.EyeTrackerData();
            DataWindow.Text += oculist.DisplayAreaData();
            DataWindow.Text += oculist.ActualDisplayScreenData();

            baseDelayInterval = updateTimer.Interval;
            TimerFrequency.Value = baseDelayInterval;
        }

        public new void Update()
        {
            #region get data from tracker and reconfiguration

            oculist.Update();

            if(LeftEyeRadio.Checked)
            {
                screenX = oculist.GetScreenLeftX();
                screenY = oculist.GetScreenLeftY();
            }
            else if(RightEyeRadio.Checked)
            {
                screenX = oculist.GetScreenRightX();
                screenY = oculist.GetScreenRightY();
            }
            else
            {
                
                //screenY += 250;
            }

            if (BothEyesRadio.Checked)
            {
                screenX = ((oculist.GetScreenLeftX() + oculist.GetScreenRightX()) / 2);
                screenY = ((oculist.GetScreenLeftY() + oculist.GetScreenRightY()) / 2);
                screenY += 150;

                headX_box.Text = ((oculist.GetHeadLeftX() + oculist.GetHeadRightX()) / 2).ToString();
                headY_box.Text = ((oculist.GetHeadLeftY() + oculist.GetHeadRightY()) / 2).ToString();
                headZ_box.Text = ((oculist.GetHeadLeftZ() + oculist.GetHeadRightZ()) / 2).ToString();
            }
            else
            {
                headX_box.Text = oculist.GetHeadLeftX().ToString() + " " + oculist.GetHeadRightX().ToString();
                headY_box.Text = oculist.GetHeadLeftY().ToString() + " " + oculist.GetHeadRightY().ToString();
                headZ_box.Text = oculist.GetHeadLeftX().ToString() + " " + oculist.GetHeadRightZ().ToString();
            }


            if (checkLocalScreenPos.Checked)
            {
                textReposition.Checked = false;
                Reposition.Checked = false;
                screenX = screenX - this.Left;
                screenY = screenY - this.Top;
            }

            Recalibration(screenX, screenY);

            screenX_box.Text = " " + screenX.ToString();
            screenY_box.Text = " " + screenY.ToString();





            // Display eye Diameter
            diameter_box.Text = (int)oculist.GetLeftEyeDiameter() + "mm " + (int)oculist.GetRightEyeDiameter() + "mm";

            // Image Tracking
            if(Reposition.Checked)
            {
                checkLocalScreenPos.Checked = false;
                textReposition.Checked = false;

                screenX = screenX - imageDisplayer.Left;
                screenY = screenY - imageDisplayer.Top;
            }

            // Text Tracking
            if(textReposition.Checked)
            {
                checkLocalScreenPos.Checked = false;
                Reposition.Checked = false;
                screenX = screenX - textDisplayer.Left;
                screenY = screenY - textDisplayer.Top;
            }

            #endregion

            // Compare Data with the list to see if user noticed points
            if (startImageTracking)
            {

                ImageCursorControler((int)screenX / 2, (int)screenY/2);

                try
                {

                    for (int i = 0; i < 100; i++)
                    {
                        if ((imageFiles.PointsChecked[ImagesListBoxSorter.SelectedIndex, i] != true && trackerComparator.CompareData(screenX, screenY, imageFiles.PositionX, imageFiles.PositionY, (int)ImagesListBoxSorter.SelectedIndex, i, (int)RadiusMargin.Value)) && screenX + screenY != 0)
                        {
                            ErrorMessageStatusLabel.Text = trackerComparator.AlertNoticedElement(screenX, screenY, " Image Point", i);
                            DataWindow.Text += trackerComparator.AlertNoticedElement(screenX, screenY, " Image Point", i);
                            imageFiles.PointsChecked[ImagesListBoxSorter.SelectedIndex, i] = true;
                        }
                        else if(screenX + screenY == 0)
                        {
                            ErrorMessageStatusLabel.Text = "Eye Tracker not connected!";
                        }

                        

                    }
                }
                catch
                {
                    DataWindow.Text += "Could not launch image Tracking function! Make sure you have selected image to track!\n";
                    ErrorMessageStatusLabel.Text = "Could not launch image Tracking function! Make sure you have selected image to track!";
                    btnStartTrackingImages.Text = "Start";
                    startImageTracking = false;
                }

            }

            // Compare Data to list via text displayer
            if (startTextyTracking)
            {

                TextCursorControler((int)screenX, (int)screenY);
                try
                {

                    for (int i = 0; i < 100; i++)
                    {
                        if ((textFiles.PointsChecked[TextListBoxSorter.SelectedIndex, i] != true && trackerComparator.CompareData(screenX, screenY, textFiles.PositionX, textFiles.PositionY, (int)TextListBoxSorter.SelectedIndex, i, (int)RadiusMargin.Value) == true) && screenX + screenY != 0)
                        {
                            ErrorMessageStatusLabel.Text = trackerComparator.AlertNoticedElement(screenX, screenY, " Text Point", i);
                            DataWindow.Text += trackerComparator.AlertNoticedElement(screenX, screenY, " Text Point", i);
                            textFiles.PointsChecked[TextListBoxSorter.SelectedIndex, i] = true;
                        }
                        else if (screenX + screenY == 0)
                        {
                            ErrorMessageStatusLabel.Text = "Eye Tracker not connected!";
                        }

                    }
                }
                catch
                {
                    DataWindow.Text += "Could not launch text Tracking function! Make sure you have selected text to track!\n";
                    ErrorMessageStatusLabel.Text = "Could not launch text Tracking function! Make sure you have selected text to track!";
                    btnStartTrackingText.Text = "Start";
                    startTextyTracking = false;
                }
            }

            TrackerTextBox.Click += (s, e) =>
            {
                TextCursorControler(MousePosition.X - this.Location.X - TrackerTextBox.Location.X - 12, MousePosition.Y - this.Location.Y - TrackerTextBox.Location.Y - 72);
                TextSelectPosX.Value = (MousePosition.X - this.Location.X - TrackerTextBox.Location.X - 12);
                TextSelectPosY.Value = (MousePosition.Y - this.Location.Y - TrackerTextBox.Location.Y - 72);
            };           
        }

        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
           Update();
        }

        #region Image and text adding to the system
        // Add Image
        private void Button1_Click(object sender, EventArgs e)
        {

            try
            {
                imageFiles.images.Add(contentManager.LoadImageContent());
                imageFiles.imageNames.Add(contentManager.getFileName());
                comboBoxImages.Items.Add(contentManager.getFileName());
                ImagesListBoxSorter.Items.Add(contentManager.getFileName());

            }
            catch
            {
                DataWindow.Text += "Could not add image to the program!\n";
                ErrorMessageStatusLabel.Text = "Could not add image to the program!";
            }
            DataWindow.Text += imageFiles.imageNames.Last<string>() + " Added to the program!\n";

        }

        //Add Txt File
        private void Button4_Click(object sender, EventArgs e)
        {
            try
            {
                textFiles.text.Add(contentManager.LoadTextContent());
                textFiles.textNames.Add(contentManager.getFileName());
                comboBoxText.Items.Add(contentManager.getFileName());
                TextListBoxSorter.Items.Add(contentManager.getFileName());
            }
            catch
            {
                DataWindow.Text += "Could not add text to the program!\n";
                ErrorMessageStatusLabel.Text = "Could not add text to the program!";

            }
            DataWindow.Text += textFiles.textNames.Last<string>() + " Added to the program!\n";

            //DataWindow.Text = txtNameList.Last<string>();
        }

        // Add Word Doc Unused
       private void Button6_Click(object sender, EventArgs e)
        {
          
        }
        #endregion

        #region Adding positions to images and text files
        private void BtnSetPositionImage_Click(object sender, EventArgs e)
        {
            try
            {
                imageFiles.PositionX[ImagesListBoxSorter.SelectedIndex, (int)ImageNumericStrip.Value] = (int)ImageSelectPosX.Value;
                imageFiles.PositionY[ImagesListBoxSorter.SelectedIndex, (int)ImageNumericStrip.Value] = (int)ImageSelectPosY.Value;
            }
            catch
            {
                DataWindow.Text += "Image Array out of bounds, or image Index not recognized! Add image to the list before adding position!\n";
                ErrorMessageStatusLabel.Text = "Image Array out of bounds, or image Index not recognized! Add image to the list before adding position!";

            }
        }

        private void BtnSetPositionText_Click(object sender, EventArgs e)
        {
            try
            {
                textFiles.PositionX[TextListBoxSorter.SelectedIndex, (int)TextNumericStrip.Value] = (int)TextSelectPosX.Value;
                textFiles.PositionY[TextListBoxSorter.SelectedIndex, (int)TextNumericStrip.Value] = (int)TextSelectPosY.Value;
            }
            catch
            {
                DataWindow.Text += "Text Array out of bounds, or text Index not recognized! Add text to the list before adding position!\n";
                ErrorMessageStatusLabel.Text = "Text Array out of bounds, or text Index not recognized! Add text to the list before adding position!";

            }
        }
        #endregion

        private void btnStartTrackingImages_Click(object sender, EventArgs e)
        {
            if(!startImageTracking)
            {
                btnStartTrackingImages.Text = "Stop";
                imageDisplayer.Show();
                startImageTracking = true;
            }
            else
            {
                btnStartTrackingImages.Text = "Start";
                imageDisplayer.Hide();
                startImageTracking = false;
            }
            
        }

        private void BtnStartTrackingText_Click(object sender, EventArgs e)
        {
            if (!startTextyTracking)
            {
                btnStartTrackingText.Text = "Stop";
                textDisplayer.Show();
                startTextyTracking = true;
            }
            else
            {
                btnStartTrackingText.Text = "Start";
                textDisplayer.Hide();
                startTextyTracking = false;
            }
        }

        // Remove Image Button
        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                imageFiles.images.RemoveAt(comboBoxImages.SelectedIndex);
                imageFiles.imageNames.RemoveAt(comboBoxImages.SelectedIndex);
                ImagesListBoxSorter.Items.RemoveAt(comboBoxImages.SelectedIndex);
                imageFiles.reshufleData(comboBoxImages.SelectedIndex);
                comboBoxImages.Items.RemoveAt(comboBoxImages.SelectedIndex);
                
                
            }
            catch
            {
                DataWindow.Text += "Could not remove image from the program!\n";
                ErrorMessageStatusLabel.Text = "Could not remove image from the program!";
            }
        }
        // Remove Text Button
        private void Button3_Click(object sender, EventArgs e)
        {
            try
            {
                textFiles.text.RemoveAt(comboBoxText.SelectedIndex);
                textFiles.textNames.RemoveAt(comboBoxText.SelectedIndex);
                TextListBoxSorter.Items.RemoveAt(comboBoxText.SelectedIndex);
                textFiles.reshufleData(comboBoxText.SelectedIndex);
                comboBoxText.Items.RemoveAt(comboBoxText.SelectedIndex);


            }
            catch
            {
                DataWindow.Text += "Could not remove text from the program!\n";
                ErrorMessageStatusLabel.Text = "Could not remove text from the program!";
            }
        }

        private void ImagesListBoxSorter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ImagesListBoxSorter.SelectedIndex == -1)
            {

            }
            else
            {
                pictureDisplayBox.Image = imageFiles.images[ImagesListBoxSorter.SelectedIndex];
                pictureDisplayBox.SizeMode = PictureBoxSizeMode.Zoom;
                imageDisplayer.setImage(imageFiles.images[ImagesListBoxSorter.SelectedIndex]);
            }
            
        }

        private void TextListBoxSorter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TextListBoxSorter.SelectedIndex == -1)
            {

            }
            else
            {
                TrackerTextBox.Text = textFiles.text[TextListBoxSorter.SelectedIndex];
                textDisplayer.setText(textFiles.text[TextListBoxSorter.SelectedIndex]);
            }
        }

        private void NewToolStripMenuItem_Click(object sender, EventArgs e)
        {

            NewProject newProject = new NewProject(this);
            newProject.Show();
            
            /*var confirmResult = MessageBox.Show("Current project will be deleted!",
                                     "Start a new Project?",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                EraseAllData();
            }
            else
            {
                // If 'No', do nothing.
            }*/
        }

   
        private void EraseAllData()
        {
            TextListBoxSorter.Items.Clear();
            ImagesListBoxSorter.Items.Clear();
            comboBoxText.Items.Clear();
            comboBoxImages.Items.Clear();
            // The Following Try catch methods exist in case the arrays are empty
            try
            {
                imageFiles.clearData();
            }
            catch
            {

            }

            try
            {
                textFiles.clearData();
            }
            catch
            {

            }

            DataWindow.Clear();

            // In case Image box contains no text
            try
            {
                pictureDisplayBox.Image.Dispose();
            }
            catch
            {
                
            }
            
            TrackerTextBox.Clear();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Current project will be deleted upon closing!",
                                     "Close the program?",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                this.Close();
            }
            else
            {
                // If 'No', do nothing.
            }
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About aboutForm = new About();
            aboutForm.Show();
        }

        private void ImageDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageFilesDiagnostics IFD = new ImageFilesDiagnostics(imageFiles);
            IFD.Show();
        }

        private void TextDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextFilesDiagnostics TFD = new TextFilesDiagnostics(textFiles);
            TFD.Show();
        }

        private void Reposition_CheckedChanged(object sender, EventArgs e)
        {

        }


        private void ImageCursorControler(int posX, int posY)
        {

            if(posX < 0)
            {
                posX = 0;
            }

            if(posX > 512)
            {
                posX = 512;
            }
            posX = posX + pictureDisplayBox.Location.X;

            if(posY < 0)
            {
                posY = 0;
            }

            if(posY > 345)
            {
                posY = 345;
            }

            ImageBoxCursorX.Location = new System.Drawing.Point(posX, ImageBoxCursorX.Location.Y);
            ImageBoxCursorY.Location = new System.Drawing.Point(ImageBoxCursorY.Location.X, posY);
        }

        private void TextCursorControler(int posX, int posY)
        {

            if (posX < 0)
            {
                posX = 0;
            }

            if (posX > 720)
            {
                posX = 720;
            }
            posX = posX + TrackerTextBox.Location.X;

            if (posY < 0)
            {
                posY = 0;
            }

            if (posY > 460)
            {
                posY = 460;
            }

            TextBoxCursorX.Location = new System.Drawing.Point(posX, TextBoxCursorX.Location.Y);
            TextBoxCursorY.Location = new System.Drawing.Point(TextBoxCursorY.Location.X, posY);
        }

        private void PictureDisplayBox_Click(object sender, EventArgs e)
        {
            ImageCursorControler(MousePosition.X - this.Location.X - pictureDisplayBox.Location.X - 12, MousePosition.Y - this.Location.Y - pictureDisplayBox.Location.Y - 64);
            ImageSelectPosX.Value = (MousePosition.X - this.Location.X - pictureDisplayBox.Location.X - 12) *2;
            ImageSelectPosY.Value = (MousePosition.Y - this.Location.Y - pictureDisplayBox.Location.Y - 64) *2;
        }

        private void LeftEyeSelected_CheckedChanged(object sender, EventArgs e)
        {
            LeftEyeRadio.Checked = true;  
        }

        private void RightEyeSelected_CheckedChanged(object sender, EventArgs e)
        {
            RightEyeRadio.Checked = true;
        }

        private void BothEyesCalibrate_CheckedChanged(object sender, EventArgs e)
        {
            BothEyesRadio.Checked = true;
        }


        public void Recalibration(int x, int y)
        {
            //Displaying current EyeTrackerData
            CalibrateEyeXBox.Text = x.ToString();
            CalibrateEyeYBox.Text = y.ToString();

            XCalibrationLine.Location = new System.Drawing.Point(x, XCalibrationLine.Location.Y);
            YCalibrationLine.Location = new System.Drawing.Point(YCalibrationLine.Location.X, y);

            screenX += (int)X_Offset.Value;
            screenY += (int)X_Offset.Value;

            updateTimer.Interval = baseDelayInterval;
        }

        private void questionsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void checkRecordData_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void TimerFrequency_Scroll(object sender, EventArgs e)
        {
            baseDelayInterval = TimerFrequency.Value;
        }

        public void setVisibleDataWindow()
        {
            MainDataWindow.Visible = true;
        }
    }
}
