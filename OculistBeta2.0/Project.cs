﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OculistBeta2._0
{
    public class Project
    {

        ContentManager contentManager = new ContentManager();

        readonly string PROJECTNAME;

        static readonly int maxSize = 0x64;

        public int[,] imagePositionX = new int[maxSize, maxSize];
        public int[,] imagePositionY = new int[maxSize, maxSize]; // image ID position; Imag﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OculistBeta2._0
{
    public class Project
    {

        ContentManager contentManager = new ContentManager();

        readonly string PROJECTNAME;

        static readonly int maxSize = 0x64;

        public int[,] imagePositionX = new int[maxSize, maxSize];
        public int[,] imagePositionY = new int[maxSize, maxSize]; // image ID position; Image ID, position Y;
        public int[,] textPositionX = new int[maxSize, maxSize];
        public int[,] textPositionY = new int[maxSize, maxSize];

        public string[,] textQuestions = new string[maxSize, maxSize];
        public string[,] imageQuestions = new string[maxSize, maxSize];

        public List<Image> images = new List<Image>();
        public List<string> text = new List<string>();

        public List<string> imageNames = new List<string>();
        public List<string> textNames = new List<string>();

        public bool[,] imagePointsChecked = new bool[maxSize, maxSize]; // image ID,point ID
        public bool[,] textPointsChecked = new bool[maxSize, maxSize]; // text ID,point ID



        public Project(string name, bool newProject)
        {
            PROJECTNAME = name;

            if(newProject)
            {
                CreateNewProject(name);
            }
        }

        public void CreateNewProject(string ProjectName)
        {
            Directory.CreateDirectory("Projects/" + ProjectName + "/Data/Images");
            Directory.CreateDirectory("Projects/" + ProjectName + "/Data/Text");
            Directory.CreateDirectory("Projects/" + ProjectName + "/Data/Output");
            File.Create("Projects/" + ProjectName + "/" + ProjectName + ".oculist");
        }

        public void AddImage()
        {
            contentManager.LoadImageContent(PROJECTNAME);
            images.Add(new Bitmap(contentManager.GetFileName()));
            imageNames.Add(Path.GetFileName(contentManager.GetFileName()));
        }

        public void AddText()
        {
            contentManager.LoadTextContent(PROJECTNAME);
            string textData;

            using (StreamReader reader = new StreamReader(contentManager.GetFileName()))
            {
                textData = reader.ReadToEnd();
            }

            text.Add(textData);
            textNames.Add(Path.GetFileName(contentManager.GetFileName()));
            
        }

        public void RemoveImage(int index)
        {
            
            images.RemoveAt(index);
            imageNames.RemoveAt(index);
            reshufleData(index, imagePositionX, imagePositionY, imagePointsChecked,imageQuestions);
        }

        public void RemoveText(int index)
        {
            text.RemoveAt(index);
            textNames.RemoveAt(index);
            reshufleData(index, textPositionX, textPositionY, textPointsChecked,textQuestions);
        }


        public void clearData(int[,] PositionX, int[,] PositionY, bool[,] PointsChecked)
        {
            images.Clear();
            imageNames.Clear();

            text.Clear();
            textNames.Clear();

            Array.Clear(PositionX, 0x00, PositionX.Length);
            Array.Clear(PositionY, 0x00, PositionY.Length);
            Array.Clear(PointsChecked, 0x00, PointsChecked.Length);
        }

        // Function called when an entry is deleted from the list
        public void reshufleData(int index, int[,] PositionX, int[,] PositionY, bool[,] PointsChecked, string[,] Questions)
        {
            for (int i = index; i < maxSize - 0x01; i++)
            {
                for (int j = 0x00; j < maxSize; j++)
                {
                    PositionX[i, j] = PositionX[i + 0x01, j];
                    PositionY[i, j] = PositionY[i + 0x01, j];
                    Questions[i, j] = Questions[i + 0x01, j];
                    PointsChecked[i, j] = PointsChecked[i + 0x01, j];
                }
            }
        }

        public void swapData(int index1, int index2, int[,] PositionX, int[,] PositionY, bool[,] PointsChecked)
        {
            int dummy;
            bool booly;
            for (int j = 0x00; j < maxSize; j++)
            {
                dummy = PositionX[index1, j];
                PositionX[index1, j] = PositionX[index2, j];
                PositionX[index2, j] = dummy;

                dummy = PositionY[index1, j];
                PositionY[index1, j] = PositionY[index2, j];
                PositionY[index2, j] = dummy;

                booly = PointsChecked[index1, j];
                PointsChecked[index1, j] = PointsChecked[index2, j];
                PointsChecked[index2, j] = booly;
            }
        }


    }
}
