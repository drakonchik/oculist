﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OculistBeta2._0
{
    public partial class MainWindow : Form
    {

        Oculist oculist = new Oculist();
        EyeDataFormating reformater = new EyeDataFormating();
        
        Project project;

        Recordkeeper keeper = new Recordkeeper();

        TrackingComparator trackingComparator = new TrackingComparator();

        ImageTracking imageTracking = new ImageTracking();
        TextTracking textTracking = new TextTracking();

        public bool participantAgree = false;
        private bool startImageTracking = false;
        private bool startTextTracking = false;


        // Constructor
        public MainWindow()
        {
            InitializeComponent();
            if(oculist.TrackerConnectedStatus() == true)
            {
                checkBoxDevelopmentMode.Checked = true;
                checkBoxDevelopmentMode.Enabled = false;
            }
        }

        // Update
        private void Update()
        {
            // Updating the values from the Eye Tracker to the text Box
            oculist.Update();
            UpdateEyeTrackingData();
            
            if (participantAgree && startImageTracking)
            {
                reformater.UpdateImageWindow(oculist.GetScreenLeftX(), oculist.GetScreenLeftY(),
                oculist.GetScreenRightX(), oculist.GetScreenRightY(), imageTracking);
                try
                {

                    for (int i = 0; i < 100; i++)
                    {
                        if ((project.imagePointsChecked[imageExperimentListBox.SelectedIndex, i] != true && trackingComparator.CompareData(reformater.getscreenX(), reformater.getscreenY(), project.imagePositionX, ﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OculistBeta2._0
{
    public partial class MainWindow : Form
    {

        Oculist oculist = new Oculist();
        EyeDataFormating reformater = new EyeDataFormating();
        
        Project project;

        Recordkeeper keeper = new Recordkeeper();

        TrackingComparator trackingComparator = new TrackingComparator();

        ImageTracking imageTracking = new ImageTracking();
        TextTracking textTracking = new TextTracking();

        public bool participantAgree = false;
        private bool startImageTracking = false;
        private bool startTextTracking = false;


        // Constructor
        public MainWindow()
        {
            InitializeComponent();
            if(oculist.TrackerConnectedStatus() == true)
            {
                checkBoxDevelopmentMode.Checked = true;
                checkBoxDevelopmentMode.Enabled = false;
            }
        }

        // Update
        private void Update()
        {
            // Updating the values from the Eye Tracker to the text Box
            oculist.Update();
            UpdateEyeTrackingData();
            
            if (participantAgree && startImageTracking)
            {
                reformater.UpdateImageWindow(oculist.GetScreenLeftX(), oculist.GetScreenLeftY(),
                oculist.GetScreenRightX(), oculist.GetScreenRightY(), imageTracking);
                try
                {

                    for (int i = 0; i < 100; i++)
                    {
                        if ((project.imagePointsChecked[imageExperimentListBox.SelectedIndex, i] != true && trackingComparator.CompareData(reformater.getscreenX(), reformater.getscreenY(), project.imagePositionX, project.imagePositionY, (int)imageExperimentListBox.SelectedIndex, i, (int)numericUpDownRadius.Value)) && reformater.getscreenX() + reformater.getscreenY() != 0)
                        {

                            project.imagePointsChecked[imageExperimentListBox.SelectedIndex, i] = true;
                            keeper.Update(reformater.getscreenX(), reformater.getscreenY(), oculist.GetScreenLeftX(), oculist.GetScreenLeftY(), oculist.GetScreenRightX(), oculist.GetScreenRightY(), false, false, true, false, "Found_point_at_" + reformater.getscreenX() + "_" + reformater.getscreenY() + "", false, false);
                        }
                        else if (reformater.getscreenX() + reformater.getscreenY() == 0)
                        {
                            MessageBox.Text = "Eye Tracker not connected!";
                            keeper.Update(reformater.getscreenX(), reformater.getscreenY(), oculist.GetScreenLeftX(), oculist.GetScreenLeftY(), oculist.GetScreenRightX(), oculist.GetScreenRightY(), false, false, false, false, "Found_point_at_" + reformater.getscreenX() + "_" + reformater.getscreenY() + "", false, false);
                        }
                        

                    }
                }
                catch
                {
                    MessageBox.Text += "Could not launch image Tracking function! Make sure you have selected image to track!\n";
                    butonShowImageTrackingWindow.Text = "Show";
                    imageTracking.Hide();
                    startImageTracking = false;
                }
            }
            else if(participantAgree && startTextTracking)
            {
                reformater.UpdateImageWindow(oculist.GetScreenLeftX(), oculist.GetScreenLeftY(),
                oculist.GetScreenRightX(), oculist.GetScreenRightY(), imageTracking);

                try
                {

                    for (int i = 0; i < 100; i++)
                    {
                        if ((project.textPointsChecked[textExperimentListBox.SelectedIndex, i] != true && trackingComparator.CompareData(reformater.getscreenX(), reformater.getscreenY(), project.textPositionX, project.textPositionY, (int)textExperimentListBox.SelectedIndex, i, (int)numericUpDownRadius.Value)) && reformater.getscreenX() + reformater.getscreenY() != 0)
                        {

                            project.textPointsChecked[textExperimentListBox.SelectedIndex, i] = true;
                        }
                        else if (reformater.getscreenX() + reformater.getscreenY() == 0)
                        {
                            MessageBox.Text = "Eye Tracker not connected!";
                        }

                    }
                }
                catch
                {
                    MessageBox.Text += "Could not launch image Tracking function! Make sure you have selected image to track!\n";
                    butonShowImageTrackingWindow.Text = "Show";
                    imageTracking.Hide();
                    startImageTracking = false;
                }
            }
            else
            {
                reformater.Update(oculist.GetScreenLeftX(), oculist.GetScreenLeftY(),
                oculist.GetScreenRightX(), oculist.GetScreenRightY(), checkBoxLocalScreenPosition.Checked, this);
            }
            reformater.readjustData((int)numericUpDownScreenEyeTrackingXOffset.Value, (int)numericUpDownScreenEyeTrackingYOffset.Value);
            timerDelayTextBox.Text = timerDelaySlider.Value.ToString();
            boxTextExperimentControls.Enabled = participantAgree;
            boxImageExperimentControls.Enabled = participantAgree;

            if(!checkBoxDevelopmentMode.Checked)
            {
                handleXYDrawing(reformater.getscreenX(), reformater.getscreenY());
            }

            keeper.Update(reformater.getscreenX(), reformater.getscreenY(), oculist.GetScreenLeftX(), oculist.GetScreenLeftY(), oculist.GetScreenRightX(), oculist.GetScreenRightY(), false, false, false, false, "Found_point_at_" + reformater.getscreenX() + "_" + reformater.getscreenY() + "", true, false);
            controlTextBox_Click();

            if(checkboxRecordLeftEyeData.Checked == true)
            {
                keeper.recordEyes(reformater.getscreenX(), reformater.getscreenY());
            }

            if(checkboxRecordRightEye.Checked == true)
            {
                keeper.saveRecordingToCSV2();
            }
        }

        // Tick the main window
        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            Update();
        }



        public void UpdateEyeTrackingData()
        {
            rightEyePupilDiameterTextBox.Text = oculist.GetRightEyeDiameter().ToString();
            leftEyePupilDiameterTextBox.Text = oculist.GetLeftEyeDiameter().ToString();

            leftEyeXTextBox.Text = oculist.GetScreenLeftX().ToString();
            leftEyeYTextBox.Text = oculist.GetScreenLeftY().ToString();

            rightEyeXTextBox.Text = oculist.GetScreenRightX().ToString();
            rightEyeYTextBox.Text = oculist.GetScreenRightY().ToString();

            leftHeadEyeXTextBox.Text = oculist.GetHeadLeftX().ToString();
            leftHeadEyeYTextBox.Text = oculist.GetHeadLeftY().ToString();
            leftHeadEyeZTextBox.Text = oculist.GetHeadLeftZ().ToString();

            rightHeadEyeXTextBox.Text = oculist.GetHeadRightX().ToString();
            rightHeadEyeYTextBox.Text = oculist.GetHeadRightY().ToString();
            rightHeadEyeZTextBox.Text = oculist.GetHeadRightZ().ToString();

            bothEyesXTextBox.Text = reformater.getscreenX().ToString();
            bothEyesYTextBox.Text = reformater.getscreenY().ToString();
        }

        // Information Controller for the image lists and or data

        // This function creates a new project file
        public void createNewProject(string projectnamey)
        {
            project = new Project(projectnamey, true);
            MainTabControl.Enabled = true;
            MessageBox.Enabled = true;
        }

        private void NewProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewProject projecty = new NewProject(this);
            projecty.Show();
        }

        private void ButtonAddImage_Click(object sender, EventArgs e)
        {
            try
            {
                project.AddImage();
                comboBoxImageFiles.Items.Add(project.imageNames.Last<string>());
                imageDevelopmentListBox.Items.Add(project.imageNames.Last<string>());
                imageExperimentListBox.Items.Add(project.imageNames.Last<string>());
            }
            catch
            {
                MessageBox.Text += "Could not add a new Image file!\n";
            }
            
        }

        private void ButtonRemoveImage_Click(object sender, EventArgs e)
        {
            try
            {
                project.RemoveImage(comboBoxImageFiles.SelectedIndex);
            }
            catch
            {

            }
            
        }

        private void ButtonAddText_Click(object sender, EventArgs e)
        {
            try
            {
                project.AddText();
                comboBoxTextFiles.Items.Add(project.textNames.Last<string>());
                textDevelopmentListBox.Items.Add(project.textNames.Last<string>());
                textExperimentListBox.Items.Add(project.textNames.Last<string>());
            }
            catch
            {
                MessageBox.Text += "Could not add a new Text file!\n";
            }
            
        }

        private void ButtonRemoveText_Click(object sender, EventArgs e)
        {
            try
            {
                project.RemoveText(comboBoxTextFiles.SelectedIndex);
            }
            catch
            {

            }
        }

        private void TimerDelaySlider_Scroll(object sender, EventArgs e)
        {
            updateTimer.Interval = timerDelaySlider.Value;
        }

        private void ButtonRequestTextPermission_Click(object sender, EventArgs e)
        {
            Agreement agreement = new Agreement(this);
            agreement.Show();
        }

        private void ButtonRequestImagePermission_Click(object sender, EventArgs e)
        {
            Agreement agreement = new Agreement(this);
            agreement.Show();
        }

        private void CloseProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var confirmResult = System.Windows.Forms.MessageBox.Show("Current project output data will be deleted upon closing!",
                                     "Close the program?",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                Application.Restart();
            }
            else
            {
                // If 'No', do nothing.
            }
        }

        public void AddTextForMessageBox(string message)
        {
            MessageBox.Text += message;
        }

        private void OpenProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ContentManager cm = new ContentManager();
            try
            {
                cm.LoadProjectContent();
                cm.GetFileName();
            }
            catch
            {

            }
            
        }

        private void SaveProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void RadioButtonImagesQuestions_CheckedChanged(object sender, EventArgs e)
        {
            imagesOrTextListBox.Items.Clear();
            if (radioButtonImagesQuestions.Checked)
            {
                for(int i = 0; i < project.imageNames.Count(); i++)
                {
                    imagesOrTextListBox.Items.Add(project.imageNames[i]);
                }
                textBoxQuestions.Visible = false;
            }
        }

        private void RadioButtonTextQuestions_CheckedChanged(object sender, EventArgs e)
        {
            imagesOrTextListBox.Items.Clear();
            if (radioButtonTextQuestions.Checked)
            {
                for (int i = 0; i < project.textNames.Count(); i++)
                {
                    imagesOrTextListBox.Items.Add(project.textNames[i]);
                }
            }
            textBoxQuestions.Visible = true;
        }

        private void ImagesOrTextListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            listboxTextOrImagePoints.Items.Clear();
            if (imagesOrTextListBox.SelectedIndex == -1)
            {

            }
            else
            {
                if (radioButtonTextQuestions.Checked)
                {
                    textBoxQuestions.Text = project.text[imagesOrTextListBox.SelectedIndex];

                    for (int i = 0; i < 0x64; i++)
                    {
                        listboxTextOrImagePoints.Items.Add(project.textPositionX[imagesOrTextListBox.SelectedIndex, i] +
                            " " + project.textPositionY[imagesOrTextListBox.SelectedIndex, i]);
                    }

                }
                else
                {
                    pictureBoxQuestionSetup.Image = project.images[imagesOrTextListBox.SelectedIndex];
                    pictureBoxQuestionSetup.SizeMode = PictureBoxSizeMode.Zoom;

                    for (int i = 0; i < 0x64; i++)
                    {
                        listboxTextOrImagePoints.Items.Add(project.imagePositionX[imagesOrTextListBox.SelectedIndex, i] +
                            " " + project.imagePositionY[imagesOrTextListBox.SelectedIndex, i]);
                    }
                }

            }
        }

        private void ListboxTextOrImagePoints_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listboxTextOrImagePoints.SelectedIndex == -1)
            {

            }
            else
            {
                if (radioButtonTextQuestions.Checked)
                {
                    QuestionXYHandler(project.textPositionX[imagesOrTextListBox.SelectedIndex, listboxTextOrImagePoints.SelectedIndex],
                        project.textPositionY[imagesOrTextListBox.SelectedIndex, listboxTextOrImagePoints.SelectedIndex]);
                }
                else if(radioButtonImagesQuestions.Checked)
                {
                    QuestionXYHandler(project.imagePositionX[imagesOrTextListBox.SelectedIndex, listboxTextOrImagePoints.SelectedIndex],
                        project.imagePositionY[imagesOrTextListBox.SelectedIndex, listboxTextOrImagePoints.SelectedIndex]);
                }
            }
        }

        private void ImageDevelopmentListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(imageDevelopmentListBox.SelectedIndex == -1)
            {

            }
            else
            {
                controlPictureBox.Image = project.images[imageDevelopmentListBox.SelectedIndex];
                controlPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        private void ButtonSetTextPoint_Click(object sender, EventArgs e)
        {
            try
            {
                project.textPositionX[textDevelopmentListBox.SelectedIndex, (int)textDevPointNumber.Value] = (int)textDevPosX.Value;
                project.textPositionY[textDevelopmentListBox.SelectedIndex, (int)textDevPointNumber.Value] = (int)textDevPosY.Value;
            }
            catch
            {
                MessageBox.Text += "Array out of bounds, or image Index not recognized! Add image to the list before adding position!\n";
            }
            

        }


        public void handleXYDrawing(int x, int y)
        {
            imageXYHandler(x, y);
            textXYHandler(x, y);
            correctorXYhandler(x, y);
        }

        public void QuestionXYHandler(int posX, int posY)
        {
            int minposX = 406;
            int maxposX = questionYAxis.Width;
            int minposY = 6;
            int maxposY = questionXAxis.Height;

            if (posX < minposX)
            {
                posX = minposX;
            }

            if (posX > maxposX)
            {
                posX = maxposX;
            }
            posX = posX + MainTabControl.Location.X + minposX-100;

            if (posY < minposY)
            {
                posY = minposY;
            }

            if (posY > maxposY)
            {
                posY = maxposY;
            }


            questionXAxis.Location = new System.Drawing.Point(posX, questionXAxis.Location.Y);
            questionYAxis.Location = new System.Drawing.Point(questionYAxis.Location.X, posY);
        }

        public void imageXYHandler(int posX, int posY)
        {
            int minposX = 7;
            int maxposX = imageYAxis.Width;
            int minposY = 5;
            int maxposY = imageXAxis.Height;

            if (posX < minposX)
            {
                posX = minposX;
            }

            if (posX > maxposX)
            {
                posX = maxposX;
            }
            posX = posX + MainTabControl.Location.X + 27;

            if (posY < minposY)
            {
                posY = minposY;
            }

            if (posY > maxposY)
            {
                posY = maxposY;
            }
            imageXAxis.Location = new System.Drawing.Point(posX, imageXAxis.Location.Y);
            imageYAxis.Location = new System.Drawing.Point(imageYAxis.Location.X, posY);
        }

        public void textXYHandler(int posX, int posY)
        {
            int minposX = 7;
            int maxposX = textYAxis.Width;
            int minposY = 9;
            int maxposY = textXAxis.Height;

            if (posX < minposX)
            {
                posX = minposX;
            }

            if (posX > maxposX)
            {
                posX = maxposX;
            }
            posX = posX + MainTabControl.Location.X;

            if (posY < minposY)
            {
                posY = minposY;
            }

            if (posY > maxposY)
            {
                posY = maxposY;
            }
            textXAxis.Location = new System.Drawing.Point(posX, textXAxis.Location.Y);
            textYAxis.Location = new System.Drawing.Point(textYAxis.Location.X, posY);
        }

        public void correctorXYhandler(int posX, int posY)
        {
            int minposX = 7;
            int maxposX = correctionYAxis.Width;
            int minposY = 5;
            int maxposY = correctionXAxis.Height;

            if (posX < minposX)
            {
                posX = minposX;
            }

            if (posX > maxposX)
            {
                posX = maxposX;
            }
            posX = posX + MainTabControl.Location.X;

            if (posY < minposY)
            {
                posY = minposY;
            }

            if (posY > maxposY)
            {
                posY = maxposY;
            }
            correctionXAxis.Location = new System.Drawing.Point(posX, correctionXAxis.Location.Y);
            correctionYAxis.Location = new System.Drawing.Point(correctionYAxis.Location.X, posY);
        }

        private void controlPictureBox_Click(object sender, EventArgs e)
        {
            try
            {
                imageXYHandler(MousePosition.X - this.Location.X - controlPictureBox.Location.X - 57, MousePosition.Y - this.Location.Y - controlPictureBox.Location.Y - 70);
                imageDevPosX.Value = (MousePosition.X - this.Location.X - controlPictureBox.Location.X - 57);
                imageDevPosY.Value = (MousePosition.Y - this.Location.Y - controlPictureBox.Location.Y - 70);
            }
            catch
            {

            }
            
        }

        private void butonShowTextTrackingWindow_Click(object sender, EventArgs e)
        {
            if (!startTextTracking)
            {
                butonShowImageTrackingWindow.Text = "Hide";
                textTracking.Show();
            }
            else
            {
                butonShowTextTrackingWindow.Text = "Show";
                askQuestions();
                textTracking.Hide();
            }
        }

        private void buttonSetImagePoint_Click(object sender, EventArgs e)
        {
            try
            {
                project.imagePositionX[imageDevelopmentListBox.SelectedIndex, (int)imageDevPointNumber.Value] = (int)imageDevPosX.Value;
                project.imagePositionY[imageDevelopmentListBox.SelectedIndex, (int)imageDevPointNumber.Value] = (int)imageDevPosY.Value;
            }
            catch
            {
                MessageBox.Text += "Array out of bounds, or image Index not recognized! Add image to the list before adding position!\n";
            }

        }

        private void butonShowImageTrackingWindow_Click(object sender, EventArgs e)
        {

            if(!startImageTracking)
            {
                butonShowImageTrackingWindow.Text = "Hide";
                imageTracking.Show();
            }
            else
            {
                butonShowImageTrackingWindow.Text = "Show";
                imageTracking.Hide();
                askQuestions();

            }
            
        }

        private void controlTextBox_Click()
        {
            controlTextBox.Click += (s, e) =>
            {
                textXYHandler(MousePosition.X - this.Location.X - controlTextBox.Location.X - 37, MousePosition.Y - this.Location.Y - controlTextBox.Location.Y - 70);
                textDevPosX.Value = (MousePosition.X - this.Location.X - controlTextBox.Location.X - 37);
                textDevPosY.Value = (MousePosition.Y - this.Location.Y - controlTextBox.Location.Y - 70);
            };
        }

        private void buttonSetQuestion_Click(object sender, EventArgs e)
        {
            if (listboxTextOrImagePoints.SelectedIndex == -1)
            {

            }
            else
            {
                if (radioButtonTextQuestions.Checked)
                {
                    project.textQuestions[imagesOrTextListBox.SelectedIndex, listboxTextOrImagePoints.SelectedIndex] = questionTextBox.ToString();
                }
                else if (radioButtonImagesQuestions.Checked)
                {
                    project.imageQuestions[imagesOrTextListBox.SelectedIndex, listboxTextOrImagePoints.SelectedIndex] = questionTextBox.ToString();
                }
            }
        }

        private void askQuestions()
        {
            for (int i = 0; i < 100; i++)
            {
                for(int j = 0; j < 100; j++)
                {
                    if (project.imagePointsChecked[i, j] != true && project.imageQuestions[i,j] != null)
                    {
                        var confirmResult = System.Windows.Forms.MessageBox.Show(project.imageQuestions[i, j],
                                     "Question?",
                                     MessageBoxButtons.YesNo);
                        if (confirmResult == DialogResult.Yes)
                        {
                            // if yes, do something.
                        }
                        else
                        {
                            // If 'No', do something.
                        }
                    }
                }
            }
        }

        private void imageExperimentListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (imageExperimentListBox.SelectedIndex == -1)
            {

            }
            else
            {
                controlPictureBox.Image = project.images[imageExperimentListBox.SelectedIndex];
                controlPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                imageTracking.setImage(project.images[imageExperimentListBox.SelectedIndex]);
            }
        }
    }
}
