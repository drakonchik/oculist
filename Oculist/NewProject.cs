﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oculist
{
    public partial class NewProject : Form
    {

        OpenFileDialog openFileDialog = new OpenFileDialog();
        Form1 mainForm;

        public NewProject(Form1 Formy)
        {
            mainForm = Formy;
            InitializeComponent();
            ProjectName.Text = "New Projecty";
            
        }

        private void CreateNewProjectButton_Click(object sender, EventArgs e)
        {
            if (Directory.Exists("Projects/" + ProjectName.Text + "/Data/Images"))
            {
                ProjectName.Text = "ProjectAlreadyExists";
            }
            else
            {
                Directory.CreateDirectory("Projects/" + ProjectName.Text + "/Data/Images");
    ﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oculist
{
    public partial class NewProject : Form
    {

        OpenFileDialog openFileDialog = new OpenFileDialog();
        Form1 mainForm;

        public NewProject(Form1 Formy)
        {
            mainForm = Formy;
            InitializeComponent();
            ProjectName.Text = "New Projecty";
            
        }

        private void CreateNewProjectButton_Click(object sender, EventArgs e)
        {
            if (Directory.Exists("Projects/" + ProjectName.Text + "/Data/Images"))
            {
                ProjectName.Text = "ProjectAlreadyExists";
            }
            else
            {
                Directory.CreateDirectory("Projects/" + ProjectName.Text + "/Data/Images");
                Directory.CreateDirectory("Projects/" + ProjectName.Text + "/Data/Text");
                Directory.CreateDirectory("Projects/" + ProjectName.Text + "/Graphics/Research");
                File.Create("Projects/" + ProjectName.Text + "/" + ProjectName.Text + ".oculist");
                mainForm.setVisibleDataWindow();
                this.Close();
            }
            
        }

        private void CancelNewProject_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
